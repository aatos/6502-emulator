import fs from 'fs';

var readCHR = function() {
    // TODO: can't understand why this doesn't compile without 'Buffer'
    var buf = fs.readFileSync("simpler-helloworld.nes");

    let headerSize = 16;

    let chrStart = headerSize + buf[4] * 16384;
    let chrEnd = chrStart + buf[5] * 8192;

    return buf.slice(chrStart, chrEnd);
}

module.exports = readCHR;
