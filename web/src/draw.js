import readCHR from './nes';

export default function draw() {
    var canvas = document.querySelector('.nesCanvas');
    var width = canvas.width = window.innerWidth;
    var height = canvas.height = window.innerHeight;

    if (!canvas.getContext) {
        return;
    }

    let ctx = canvas.getContext('2d');

    let kMaxTextures_ = 512;
    let kTextureWidth_ = 8;
    let kTextureHeight_ = 8;

    let sprites = [];

    var buf = readCHR();

    for (let t = 0; t < kMaxTextures_; ++t) {
        let nesSprite = ctx.createImageData(kTextureWidth_, kTextureHeight_);

        let chrTile = buf.slice(t * (kTextureWidth_ + kTextureHeight_));
        if ((t + 1) * (kTextureWidth_ + kTextureHeight_) > buf.length) {
            console.log("CHR was too short!");
            return false;
        }

        for (let i = 0; i < kTextureWidth_; ++i) {
            for (let j = 0; j < kTextureHeight_; ++j) {
                // FIXME: explain CHR format.. and fix the following abomination
                let pixelColor = (((chrTile[i + 8] & (0x80 >> (j % 8))) ? 2 : 0) |
                                  ((chrTile[i] & (0x80 >> (j % 8))) ? 1 : 0));
                let pixel = (i * kTextureWidth_ + j) * 4;

                switch(pixelColor) {
                    case 3:
                        nesSprite.data[pixel + 0] = 0xff;
                        break;
                    case 2:
                        nesSprite.data[pixel + 1] = 0xff;
                        break;
                    case 1:
                        nesSprite.data[pixel + 2] = 0xff;
                        break;
                }

                nesSprite.data[pixel + 3] = 0xff;
            }
        }
        sprites.push(nesSprite);
    }

    ctx.fillRect(0, 0, width, height);

    var x = 0;
    var y = 50;
    var i = 0;
    for (const imageData of sprites) {
        ctx.putImageData(imageData, x, y);
        x += 8;
        i++;
        if (i % 30 == 0) {
            x = 0;
            y += 10;
        }
    }

    return true;
}

function draw2() {
    var rectangle = new Path2D();
    rectangle.rect(10, 10, 50, 50);

    var canvas = document.querySelector('.nesCanvas');
    var width = canvas.width = window.innerWidth;
    var height = canvas.height = window.innerHeight;

    if (!canvas.getContext) {
        return;
    }

    var i = 0;
    var step = 10;

    function recur() {
        var ctx = document.querySelector('.nesCanvas').getContext('2d');
        ctx.clearRect(0, 0, width, height);


        ctx.save();

        ctx.translate(i, 0);
        ctx.stroke(rectangle);

        ctx.restore();

        if (i + step < 0 || i + step > width) {
            step *= -1;
        }

        i += step;

        window.requestAnimationFrame(recur);
    };

    recur();
}

function draw1() {
    var canvas = document.querySelector('.nesCanvas');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    if (!canvas.getContext) {
        return;
    }

    var ctx = canvas.getContext('2d');

    ctx.save();

    ctx.fillStyle = 'rgba(255, 165, 0, 1)';
    ctx.strokeStyle = 'rgba(255, 0, 0, 0.5)';

    var rectangle = new Path2D();
    rectangle.rect(10, 10, 50, 50);

    var circle = new Path2D();
    circle.moveTo(125, 35);
    circle.arc(100, 35, 25, 0, 2 * Math.PI);

    ctx.save();
    ctx.rotate((Math.PI / 180) * 25);
    ctx.stroke(rectangle);
    ctx.restore();

    ctx.save();
    ctx.fill(circle);

    ctx.translate(20, 20);
    ctx.scale(0.5, 0.5);
    ctx.fill(circle);

    ctx.translate(20, 20);
    ctx.scale(0.5, 0.5);
    ctx.fill(circle);

    ctx.translate(20, 20);
    ctx.scale(0.5, 0.5);
    ctx.fill(circle);

    ctx.restore();
    ctx.translate(60, 60);
    // mirror horizontally
    ctx.scale(-1, 1);
    ctx.stroke(rectangle);
}
