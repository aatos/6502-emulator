## 6502-emulator

Emulator for 6502 CPU and 2C02 PPU using C++ and SFML.

No audio support and no support for some "advanced" PPU functionality
(e.g. Sprite 0 hit and scrolling), but should work with simple ROMs. Supports
150 out of 151 CPU instructions.

Some test ROMs available in [nes-demos](https://gitlab.com/aatos/nes-demos).

### Building

Build the emulator with:
``` sh
scons
```

Get help with the build targets:
``` sh
$ scons --help
scons: Reading SConscript files ...
scons: done reading SConscript files.

=========== 6502-emulator ===========
'scons [emu]' to build the emulator.
'scons test' to build the unit tests and run them, cppcheck and scan-build.

Add 'release=1' for optimization and not including the debug symbols.
Add 'use_san=1' to compile with clang sanitizers.
Add 'print_inst=1' to print all executed instructions.
Add 'skip_scanbuild=1' to skip scan-build when testing.
```

Makefile is also included for convenience with same targets (`emu` and `test`)
and almost the same functionality.

#### Running

``` sh
./emu <.nes-file>
```

For example, running [pong.nes](https://gitlab.com/aatos/nes-demos/raw/master/pong/pong.nes):

``` sh
./emu pong.nes
```

![pong](http://aatosjalo.com/images/posts/nes-changelog/pong_v5.gif)

#### Test

Tests are compiled and ran with:

``` sh
scons test # or: make test
```

This compiles `gtest` unit tests and runs them with `valgrind` (if it is
available). In addition, it runs `cppcheck` and `scan-build` on `src` folder.

This (without `scan-build`) is also executed by the gitlab-ci when a commit is
pushed.

### Missing

* Emulation:
  * Sprite 0 hit
  * PPU scrolling
  * APU (Audio processing unit)
  * BRK instruction
  * Save states
* GUI:
  * Toolbar with buttons, such as File->{Open, Reset, Exit} and About
  * Configurable controls

### Controls

| Button   | Key (P1) | Key (P2) |
|----------|----------|----------|
| Up       | Up       | K        |
| Down     | Down     | J        |
| Left     | Left     | H        |
| Right    | Right    | L        |
| A        | A        | Y        |
| B        | S        | U        |
| Start    | Q        | I        |
| Select   | W        | O        |

### Changelog

* v0.5
  * Changed NMI handling from a timer based method to instruction based.
  * Sprites can be flipped according to their attributes.
  * Supports ROMs larger than 8KB.
  * Handles 150 CPU instructions.
  * Changed make to scons.
  * Fixed a lot of issues here and there.

  ![simpler-helloworld-v5](http://aatosjalo.com/images/posts/nes-changelog/simpler_v5.jpg)
  ![helloworld-v5](http://aatosjalo.com/images/posts/nes-changelog/hello_v5.gif)
  ![pong-v5](http://aatosjalo.com/images/posts/nes-changelog/pong_v5.gif)

* v0.4
  * Supports [pong.nes](https://gitlab.com/aatos/nes-demos/tree/master/pong).
  * Adds support for second controller.
  * Fixed an issue with backgrounds drawn with wrong palette.
  * Supports 33 CPU instructions.

* v0.3
  * Supports [helloworld.nes](https://gitlab.com/aatos/nes-demos/tree/master/helloworld).
  * Adds support for NMI.
  * Adds support for DMA.
  * Adds support for palettes.
  * Adds support for one controller.
  * Supports 24 CPU instructions.

  ![simpler-helloworld-v3](http://aatosjalo.com/images/posts/nes-changelog/simpler_v3.jpg) ![helloworld-v3](http://aatosjalo.com/images/posts/nes-changelog/hello_v3.gif)

* v0.2
  * Supports [simpler-helloworld.nes](https://gitlab.com/aatos/nes-demos/tree/master/simpler-helloworld).
  * PPU can now draw some sprites to the screen.
  * Supports 18 CPU instructions.

  ![simpler-helloworld-v2](http://aatosjalo.com/images/posts/nes-changelog/simpler_v2.jpg)

* v0.1
  * CPU can execute the main part (without NMI) of [helloworld.nes](https://gitlab.com/aatos/nes-demos/tree/master/helloworld).   
  * Supports 18 CPU instructions.
