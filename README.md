## 6502-emulator

**Moved to https://gitlab.com/aatos/6502-emulator**

Emulator for 6502 CPU and 2C02 PPU done with C++ and SFML.

### Controls

#### Player One

| Button   | Key   |
|----------|-------|
| Up       | Up    |
| Down     | Down  |
| Left     | Left  |
| Right    | Right |
| A        | A     |
| B        | S     |
| Start    | Q     |
| Select   | W     |

#### Player Two

| Button   | Key   |
|----------|-------|
| Up       | K     |
| Down     | J     |
| Left     | H     |
| Right    | L     |
| A        | Y     |
| B        | U     |
| Start    | I     |
| Select   | O     |

