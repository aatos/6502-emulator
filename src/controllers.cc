#include "controllers.h"

#include <cstring>

Controllers::Controllers() : controllers_(), isLatched_(false)
{

}

void Controllers::latch()
{
    for (int p = PlayerOne; p < PlayerMax; ++p) {
        controllers_[p].currentButton = A;
    }

    isLatched_ = true;
}

uint8_t Controllers::read(ControllerNumber c)
{
    if (c >= PlayerMax || c < PlayerOne) {
        return 0;
    }

    uint8_t status;
    switch (controllers_[c].currentButton) {
    case A:
        status = controllers_[c].a;
        break;
    case B:
        status = controllers_[c].b;
        break;
    case Select:
        status = controllers_[c].select;
        break;
    case Start:
        status = controllers_[c].start;
        break;
    case Up:
        status = controllers_[c].up;
        break;
    case Down:
        status = controllers_[c].down;
        break;
    case Left:
        status = controllers_[c].left;
        break;
    case Right:
        status = controllers_[c].right;
        break;
    default:
        status = 0;
        break;
    }

    int button = controllers_[c].currentButton + 1;
    if (button >= ButtonMax) {
        controllers_[c].currentButton = A;
        isLatched_ = false;
        std::memset(controllers_ + c, 0, sizeof(controllers_[c]));
    } else {
        controllers_[c].currentButton = static_cast<Button>(button);
    }

    return status;
}

void Controllers::press(ControllerNumber c, Button b)
{
    if (b >= ButtonMax || b < A
        || c >= PlayerMax || c < PlayerOne) {
        return;
    }

    switch (b) {
    case A:
        controllers_[c].a = 1;
        break;
    case B:
        controllers_[c].b = 1;
        break;
    case Select:
        controllers_[c].select = 1;
        break;
    case Start:
        controllers_[c].start = 1;
        break;
    case Up:
        controllers_[c].up = 1;
        break;
    case Down:
        controllers_[c].down = 1;
        break;
    case Left:
        controllers_[c].left = 1;
        break;
    case Right:
        controllers_[c].right = 1;
        break;
    case ButtonMax:
        break;
    }
}
