#pragma once

#include "windowBase.h"

#include <array>
#include <SFML/Graphics.hpp>

class Controllers;

class WindowSFML : public WindowBase {
public:
    WindowSFML(Controllers & controllers, unsigned int scale=1);

    bool draw(const std::array<uint8_t, 256> & oamData,
              const std::array<uint8_t, 16383> & vram,
              const PPU::ControlStatus & attributes);

    bool initTextures(const uint8_t * chrStart,
                      size_t chrSize);

    void handleControls();

private:

    struct __attribute__((packed)) RGBAData {
        uint8_t r;
        uint8_t g;
        uint8_t b;
        uint8_t a;
    };

    struct NESSprite {
        sf::Texture texture;
        sf::Sprite sprite;
        uint8_t color;
        sf::Uint8 pixels[textureWidth_ * textureHeight_ * sizeof(RGBAData)];
        uint8_t pixelColors[textureWidth_ * textureHeight_];

        void changeColor(const std::array<uint16_t, 4> & paletteAddresses,
                         uint8_t palette,
                         const std::array<uint8_t, 16383> & vram);

        void flip(bool verticalFlip,
                  bool horizontalFlip);
    };

    void drawSprites(const std::array<uint8_t, 256> & oamData,
                     const std::array<uint8_t, 16383> & vram,
                     bool secondPatternTable,
                     bool drawPriorities);

    void drawBackground(const std::array<uint8_t, 16383> & vram,
                        bool secondPatternTable);

    unsigned int scale_;
    sf::RenderWindow window_;
    std::array<NESSprite, maxTextures_> sprites_;
    bool isTexturesInitialized_;
    Controllers & controllers_;
};
