#include "ines.h"

#include <cstring>
#include <iostream>
#include <string>

INesHeader::INesHeader() : prg_size_(0),
                           chr_size_(0),
                           vertical_mirroring_(false) {
}

// Reference from http://wiki.nesdev.com/w/index.php/INES#iNES_file_format
struct __attribute__((packed)) INesHeader::HeaderType {
    char magic[4];
    uint8_t prg_size;
    uint8_t chr_size;
    uint8_t mirroring : 1,
        battery : 1,
        trainer : 1,
        four_vram : 1,
        lower_mapper : 4;
    uint8_t vs_unisystem : 1,
        playchoice_10 : 1,
        nes2_format : 2,
        upper_mapper : 4;
    char zeroes[8];
};

int INesHeader::readHeader(std::ifstream & file, HeaderType & header)
{
    if (!file) {
        return -1;
    }

    char buf[16] = { 0 };
    file.read(buf, sizeof(buf));
    if (!file) {
        std::cerr << "Header is too short\n";
        return -1;
    }

    header = *reinterpret_cast<HeaderType *>(buf);

    return 0;
}

int INesHeader::parseHeader(std::ifstream & file)
{
    HeaderType header;

    if (readHeader(file, header)) {
        return -1;
    }

    const char * correct_magic = "NES\x1A";

    if (std::strncmp(correct_magic, header.magic,
                     strlen(correct_magic))) {
        std::cerr << "Invalid magic\n";
        return -1;
    }

    prg_size_ = header.prg_size;
    chr_size_ = header.chr_size;

    // Only support changing the mirroring flag, rest should be zero.
    if (header.battery || header.trainer || header.four_vram) {
        std::cerr << "Unsupported flags:\n"
                  << "\tBattery: " << (header.battery ? "true" : "false") << std::endl
                  << "\t512-byte trainer: " << (header.trainer ? "true" : "false") << std::endl
                  << "\tFour screen VRAM: " << (header.four_vram ? "true" : "false") << std::endl;
        return -1;
    }

    vertical_mirroring_ = header.mirroring;

    if (header.vs_unisystem || header.playchoice_10 || header.nes2_format) {
        std::cerr << "Unsupported flags:\n"
                  << "\tVS Unisystem: " << (header.vs_unisystem ? "true" : "false") << std::endl
                  << "\tPlaychoice 10: " << (header.playchoice_10 ? "true" : "false") << std::endl
                  << "\tNES2.0 format: " << (header.nes2_format ? "true" : "false") << std::endl;
        return -1;
    }

    uint8_t mapper = ((header.upper_mapper << 4) & 0xF0) | header.lower_mapper;

    if (mapper) {
        std::cerr << "Unsupported mapper: " << int(mapper) << std::endl;
        return -1;
    }

    for (uint8_t i = 0; i < sizeof(header.zeroes); ++i) {
        if (header.zeroes[i]) {
            std::cerr << "End of the header was not filled with zeroes\n";
            return -1;
        }
    }

    return 0;
}
