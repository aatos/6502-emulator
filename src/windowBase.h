#pragma once

#include "ppu.h"

#include <cinttypes>
#include <array>
#include <vector>

#ifdef __clang__
#pragma clang diagnostic push
/* It is okay to emit vtable for this pure abstract class to every translation
 * unit. */
#pragma clang diagnostic ignored "-Wweak-vtables"
#endif

class WindowBase {
public:
    virtual ~WindowBase() {}

    virtual bool draw(const std::array<uint8_t, 256> & oamData,
                      const std::array<uint8_t, 16383> & vram,
                      const PPU::ControlStatus & attributes) = 0;

    virtual bool initTextures(const uint8_t * chrStart,
                              size_t chrSize) = 0;

    virtual void handleControls() = 0;

    // 256 (TODO: or 255?) sprites times two in one CHR
    static const unsigned int kMaxTextures_ = 512;

    static const unsigned int kTextureWidth_ = 8;
    static const unsigned int kTextureHeight_ = 8;

    static const unsigned int kWindowWidth_ = 256;
    static const unsigned int kWindowHeight_ = 240;
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
