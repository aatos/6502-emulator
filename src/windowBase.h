#pragma once

#include "ppu.h"

#include <cinttypes>
#include <array>
#include <vector>

#pragma clang diagnostic push
/* It is okay to emit vtable for this pure abstract class to every translation
 * unit. */
#pragma clang diagnostic ignored "-Wweak-vtables"

class WindowBase {
public:
    virtual ~WindowBase() {}

    virtual bool draw(const std::array<uint8_t, 256> & oamData,
                      const std::array<uint8_t, 16383> & vram,
                      const PPU::ControlStatus & attributes) = 0;

    virtual bool initTextures(const uint8_t * chrStart,
                              size_t chrSize) = 0;

    // 256 (TODO: or 255?) sprites times two in one CHR
    static const unsigned int maxTextures_ = 512;

    static const unsigned int textureWidth_ = 8;
    static const unsigned int textureHeight_ = 8;

    static const unsigned int windowWidth_ = 256;
    static const unsigned int windowHeight_ = 240;
};

#pragma clang diagnostic pop
