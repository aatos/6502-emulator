#include "ppu.h"

#include <iostream>
#include <stdexcept>

#include "windowBase.h"
#include "cpu.h"

constexpr std::array<uint16_t, 4> PPU::kSpritePaletteAddresses;
constexpr std::array<uint16_t, 4> PPU::kBackgroundPaletteAddresses;

const uint16_t PPU::kNametableAddressStart;
const uint16_t PPU::kNametableSize;

PPU::PPU(WindowBase & window) : oamAddress_(0), oamData_(),
                                window_(window), inVBlank_(false),
                                shouldNMI_(false),
                                addressHigh_(true),
                                vramAddress_(0), vram_(),
                                controlStatus_(), cycle_(0)
{

}

uint8_t PPU::read(uint16_t address)
{
    switch(address) {
    case PPU::kStatusPort:
        addressHigh_ = true;
        return readStatus();
    case PPU::kControlPort:
        throw std::invalid_argument("Control port is write-only");
    case PPU::kOamAddressPort:
        return oamAddress_;
    case PPU::kOamDataPort:
        if (oamAddress_ < oamData_.size()) {
            uint8_t value = oamData_[oamAddress_];
            return value;
        } else {
            std::cerr << "Unsupported OAM address: " << address << std::endl;
            throw std::invalid_argument("Unsupported address");
        }
    case PPU::kDataPort:
        if (vramAddress_ < vram_.size()) {
            uint8_t value = vram_[vramAddress_];

            // TODO: does 32 mean other direction?
            vramAddress_ += (controlStatus_.vramIncrement ? 32 : 1);

            return value;
        }
        // Fallthrough
    }

    std::cerr << "Unsupported address: " << address << std::endl;
    throw std::invalid_argument("Unsupported address");
}

void PPU::write(uint16_t address, uint8_t value)
{
    switch(address) {
    case PPU::kControlPort:
        writeControl(value);
        break;
    case PPU::kScrollPort:
        /* Scrolling is not supported but zero is perfectly fine written to
         * scroll port */
        if (value != 0) {
            /* Scrolling is not supported. */
        }
        break;
    case PPU::kMaskPort:
        /* Mask port can be used for changing various rendering settings, but
         * not supported now. */
        break;
    case PPU::kStatusPort:
        throw std::invalid_argument("Status port is read-only");
    case PPU::kOamAddressPort:
        oamAddress_ = value;
        break;
    case PPU::kOamDataPort:
        if (oamAddress_ < oamData_.size()) {
            oamData_[oamAddress_] = value;
            ++oamAddress_;
            // TODO: add test
        } else {
            std::cerr << "Unsupported OAM address: " << address << std::endl;
            throw std::invalid_argument("Unsupported address");
        }
        break;
    case PPU::kAddressPort:
        if (addressHigh_) {
            vramAddress_ &= ~0xFF00;
            vramAddress_ |= (value << 8);
        } else {
            vramAddress_ &= ~0xFF;
            vramAddress_ |= (value);
        }

        addressHigh_ = !addressHigh_;
        break;
    case PPU::kDataPort:
        if (vramAddress_ < vram_.size()) {
            vram_[vramAddress_] = value;

            // TODO: does 32 mean other direction?
            vramAddress_ += (controlStatus_.vramIncrement ? 32 : 1);

            if (vramAddress_ >= vram_.size()) {
                vramAddress_ = 0;
            }
        } else {
            std::cerr << "Unsupported data address: " << vramAddress_ << std::endl;
            throw std::invalid_argument("Unsupported address");
        }
        break;
    default:
        std::cerr << "Unsupported address: " << address << std::endl;
        throw std::invalid_argument("Unsupported address");
    }
}

/**
 * Steps PPU a cycle forward.
 *
 * In real HW, following should happen:
 *  - A scanline should be drawn
 *  - After 339 scanlines, set inVBlank_ and call cpu.NMI if shouldNMI_ is set.
 *  - After 0->20 scanlines have passed, clear inVBlank_.
 *  - PPU runs three times faster than CPU, so this should be called three times for each CPU cycle.
 *
 * However, since we are emulating, we draw the whole picture once vblank has
 * started. At the same time, we read the controls for the CPU to read in NMI.
 *
 * Resources:
 *  - http://wiki.nesdev.com/w/index.php/PPU_frame_timing
 *  - http://wiki.nesdev.com/w/index.php/NMI
 */
bool PPU::step(bool & invokeNMI)
{
    // TODO: should be 339 but CPU doesn't calculate cycles correctly
    static const uint16_t end = 3339;

    cycle_ += 1; // TODO: should be 3

    if (cycle_ > 20 && cycle_ < end) {
        inVBlank_ = false;
    } else if (cycle_ >= end) {
        inVBlank_ = true;
        if (shouldNMI_) {
            invokeNMI = true;
        }

        cycle_ = 0;

        window_.handleControls();
        return window_.draw(oamData_, vram_, controlStatus_);
    }

    return true;
}

bool PPU::isPPUAddress(uint16_t address)
{
    switch(address) {
    case kScrollPort:
    case kControlPort:
    case kMaskPort:
    case kStatusPort:
    case kAddressPort:
    case kDataPort:
    case kOamAddressPort:
    case kOamDataPort:
        return true;
    default:
        return false;
    }
}

void PPU::getRGBFromColor(uint8_t color,
                          uint8_t & red,
                          uint8_t & green,
                          uint8_t & blue)
{
    static const struct {
        uint8_t red;
        uint8_t green;
        uint8_t blue;
    } colors[64] = {
        // Palette from http://nesdev.com/NESTechFAQ.htm#accuratepal
        { 0x80,0x80,0x80 }, { 0x00,0x3D,0xA6 }, { 0x00,0x12,0xB0 }, { 0x44,0x00,0x96 },
        { 0xA1,0x00,0x5E }, { 0xC7,0x00,0x28 }, { 0xBA,0x06,0x00 }, { 0x8C,0x17,0x00 },
        { 0x5C,0x2F,0x00 }, { 0x10,0x45,0x00 }, { 0x05,0x4A,0x00 }, { 0x00,0x47,0x2E },
        { 0x00,0x41,0x66 }, { 0x00,0x00,0x00 }, { 0x05,0x05,0x05 }, { 0x05,0x05,0x05 },
        { 0xC7,0xC7,0xC7 }, { 0x00,0x77,0xFF }, { 0x21,0x55,0xFF }, { 0x82,0x37,0xFA },
        { 0xEB,0x2F,0xB5 }, { 0xFF,0x29,0x50 }, { 0xFF,0x22,0x00 }, { 0xD6,0x32,0x00 },
        { 0xC4,0x62,0x00 }, { 0x35,0x80,0x00 }, { 0x05,0x8F,0x00 }, { 0x00,0x8A,0x55 },
        { 0x00,0x99,0xCC }, { 0x21,0x21,0x21 }, { 0x09,0x09,0x09 }, { 0x09,0x09,0x09 },
        { 0xFF,0xFF,0xFF }, { 0x0F,0xD7,0xFF }, { 0x69,0xA2,0xFF }, { 0xD4,0x80,0xFF },
        { 0xFF,0x45,0xF3 }, { 0xFF,0x61,0x8B }, { 0xFF,0x88,0x33 }, { 0xFF,0x9C,0x12 },
        { 0xFA,0xBC,0x20 }, { 0x9F,0xE3,0x0E }, { 0x2B,0xF0,0x35 }, { 0x0C,0xF0,0xA4 },
        { 0x05,0xFB,0xFF }, { 0x5E,0x5E,0x5E }, { 0x0D,0x0D,0x0D }, { 0x0D,0x0D,0x0D },
        { 0xFF,0xFF,0xFF }, { 0xA6,0xFC,0xFF }, { 0xB3,0xEC,0xFF }, { 0xDA,0xAB,0xEB },
        { 0xFF,0xA8,0xF9 }, { 0xFF,0xAB,0xB3 }, { 0xFF,0xD2,0xB0 }, { 0xFF,0xEF,0xA6 },
        { 0xFF,0xF7,0x9C }, { 0xD7,0xE8,0x95 }, { 0xA6,0xED,0xAF }, { 0xA2,0xF2,0xDA },
        { 0x99,0xFF,0xFC }, { 0xDD,0xDD,0xDD }, { 0x11,0x11,0x11 }, { 0x11,0x11,0x11 }
    };

    if (color >= (sizeof(colors) / sizeof(*colors))) {
        red = 0;
        green = 0;
        blue = 0;

        return;
    }

    red = colors[color].red;
    green = colors[color].green;
    blue = colors[color].blue;
}

uint8_t PPU::readStatus()
{
    /* Things like sprite 0 hit and sprite overflow should be here, but they are
     * not supported. */
    ControlStatus status = controlStatus_;

    status.nmi = (inVBlank_ ? 1 : 0);

    /* VBlank flag is cleared on read */
    inVBlank_ = false;

    return *reinterpret_cast<uint8_t *>(&status);
}

void PPU::writeControl(uint8_t data)
{
    ControlStatus status = *reinterpret_cast<ControlStatus *>(&data);

    if (status.spriteSize != 0) {
        std::cerr << "8x16 sprite size is not supported!" << std::endl;
    }

    if (status.baseNameTableAddress != 0) {
        std::cerr << "Unsupported base nametable address: "
                  << int(status.baseNameTableAddress) << std::endl;
    }

    controlStatus_ = status;

    shouldNMI_ = status.nmi;
}

uint8_t PPU::getAttributeIdx(uint8_t textureX, uint8_t textureY)
{
    return textureX / 4 + textureY / 4 * 8;
}

uint8_t PPU::getAttributeBits(uint8_t textureX, uint8_t textureY)
{
    return (textureX / 2 % 2) * 2 + (textureY / 2 % 2) * 4;
}
