#include <iostream>

#include "ines.h"
#include "cpu.h"
#include "ppu.h"
#include "controllers.h"
#include "windowSFML.h"

int main(int argc, char * argv[])
{
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " <.nes-file>" << std::endl;
        return -1;
    }

    const char * path = argv[1];
    std::ifstream file;
    file.open(path, std::ios_base::in | std::ios_base::binary);
    if (!file) {
        std::cerr << "Couldn't open file at " << path << std::endl;
        return -1;
    }

    INesHeader header;
    if (header.parseHeader(file)) {
        return -1;
    }

    Controllers controllers;
    WindowSFML window(controllers);

    PPU ppu = PPU(window);
    CPU cpu = CPU(ppu, controllers);

    if (!cpu.loadROM(header, file)) {
        file.close();
        return -1;
    }

    file.close();

    if (!window.initTextures(cpu.getCHRStart(), cpu.getCHRSize())) {
        return -1;
    }

    while (true) {
        if (!cpu.step()) {
            break;
        }

        bool invokeNMI = false;
        if (!ppu.step(invokeNMI)) {
            return 0;
        }

        if (invokeNMI) {
            cpu.NMI();
        }
    }

    return 0;
}
