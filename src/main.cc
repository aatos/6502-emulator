#include <iostream>

#include "ines.h"
#include "cpu.h"
#include "ppu.h"
#include "timer.h"
#include "controllers.h"
#include "windowSFML.h"

static bool timeToDraw = false; // TODO: use std::sig_atomic_t?
static void timerHandler(int)
{
    timeToDraw = true;
}

int main(int argc, char * argv[])
{
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " <.nes-file>" << std::endl;
        return -1;
    }

    const char * path = argv[1];
    std::ifstream file;
    file.open(path, std::ios_base::in | std::ios_base::binary);
    if (!file) {
        std::cout << "Couldn't open file at " << path << std::endl;
        return -1;
    }

    INesHeader header;
    if (header.parseHeader(file)) {
        return -1;
    }

    Controllers controllers;
    WindowSFML window(controllers, 2);

    PPU ppu = PPU(window);
    CPU cpu = CPU(header, file, ppu, controllers);

    window.initTextures(cpu.getCHRStart(), cpu.getCHRSize());

    file.close();

    Timer timer(timerHandler, 166666);

    while (true) {
        if (!cpu.step()) {
            break;
        }

        if (timeToDraw) {
            ppu.isNMI_ = !ppu.isNMI_;

            if (ppu.shouldNMI_) {
                cpu.NMI();

                if (!ppu.draw()) {
                    return 0;
                }

                window.handleControls();
            }

            timeToDraw = false;
        }
    }

    return 0;
}
