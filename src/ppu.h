#pragma once

#include <cinttypes>
#include <array>

class CPU;
class WindowBase;

class PPU {
public:
    explicit PPU(WindowBase & window);

    uint8_t read(uint16_t address);
    void write(uint16_t address, uint8_t value);
    bool draw();

    static bool isPPUAddress(uint16_t address);

    static void getRGBFromColor(uint8_t color,
                                uint8_t & red,
                                uint8_t & green,
                                uint8_t & blue);

    static uint8_t getAttributeIdx(uint8_t textureX, uint8_t textureY);
    static uint8_t getAttributeBits(uint8_t textureX, uint8_t textureY);

    WindowBase & window_;

    /* PPU state flags */
    bool isNMI_;
    bool shouldNMI_;
    bool addressHigh_;

    uint16_t vramAddress_;
    std::array<uint8_t, 16383> vram_; // TODO: check amount

    uint8_t oamAddress_;
    std::array<uint8_t, 256> oamData_;

    struct ControlStatus {
        uint8_t baseNameTableAddress : 2,
            vramIncrement : 1,
            spritePatternTableAddress : 1,
            backgroundPatternTableAddress : 1,
            spriteSize : 1,
            PPUMaster : 1,
            nmi : 1;
    } controlStatus_;

    static const uint16_t controlPort = 0x2000;
    static const uint16_t maskPort = 0x2001;
    static const uint16_t statusPort = 0x2002;
    static const uint16_t oamAddressPort = 0x2003;
    static const uint16_t oamDataPort = 0x2004;
    static const uint16_t scrollPort = 0x2005;
    static const uint16_t addressPort = 0x2006;
    static const uint16_t dataPort = 0x2007;

    static const uint16_t backgroundPaletteAddress = 0x3F00;
    static constexpr std::array<uint16_t, 4> backgroundPaletteAddresses = {{ 0x3F01,
                                                                             0x3F05,
                                                                             0x3F09,
                                                                             0x3F0D }};
    static constexpr std::array<uint16_t, 4> spritePaletteAddresses = {{ 0x3F11,
                                                                         0x3F15,
                                                                         0x3F19,
                                                                         0x3F1D }};
    static const uint16_t nametableAddressStart = 0x2000;
    static const uint16_t nametableSize = 960; // Bytes

    static const uint16_t secondPatternTableStart = 256;

    struct __attribute__((packed)) OAMData
    {
        uint8_t y;
        uint8_t tile;
        uint8_t attrs;
        uint8_t x;
    };

    struct __attribute__((packed)) OAMDataAttribute
    {
        uint8_t palette : 2,
            : 3,
            prio : 1,
            horizontalFlip : 1,
            verticalFlip : 1;
    };

private:
    uint8_t readControl();
    void writeControl(uint8_t data);
};
