#pragma once

#include <cinttypes>

class Controllers {
public:
    Controllers();

    enum ControllerNumber {
        PlayerOne,
        PlayerTwo,
        PlayerMax
    };

    enum Button {
        A,
        B,
        Select,
        Start,
        Up,
        Down,
        Left,
        Right,
        ButtonMax
    };

    void latch();
    uint8_t read(ControllerNumber c);
    void press(ControllerNumber c, Button b);

    struct ControllerStatus {
        Button currentButton;
        uint8_t a : 1,
            b : 1,
            select : 1,
            start : 1,
            up : 1,
            down : 1,
            left : 1,
            right : 1;
    } controllers_[PlayerMax];

private:
    bool isLatched_; // FIXME: not really used currently
};
