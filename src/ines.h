#pragma once

#include <cinttypes>
#include <fstream>

class INesHeader {
public:
    INesHeader();

    int parseHeader(std::ifstream & file);

    uint8_t prg_size_;
    uint8_t chr_size_;
    bool vertical_mirroring_;

private:
    struct __attribute__((packed)) HeaderType;

    static int readHeader(std::ifstream & file, HeaderType & header);
};
