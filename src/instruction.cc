#include "instruction.h"

#include <iostream>
#include <iomanip>

Instruction::Instruction(InstructionTypes type, AddressingModes mode) :
    type_(type), address_(0), mode_(mode)
{
}

uint8_t Instruction::getArgumentsLength() const
{
    switch (mode_) {
    case Implied:
        return 0;
    case IndirectX:
    case IndirectY:
    case Relative:
    case ZeroPage:
    case ZeroPageX:
    case Immediate:
        return 1;
    case Absolute:
    case AbsoluteX:
    case AbsoluteY:
    case Indirect:
        return 2;
    }

    std::cerr << "Unsupported addressing mode: " << mode_ << std::endl;
    return static_cast<uint8_t>(-1);
}

void Instruction::setArguments(const uint8_t * arguments)
{
    switch (mode_) {
    case Implied:
        address_ = 0;
        return;
    case IndirectX:
    case IndirectY:
    case ZeroPage:
    case ZeroPageX:
    case Immediate:
        immediate_ = arguments[0];
        return;
    case Absolute:
    case AbsoluteX:
    case AbsoluteY:
    case Indirect:
        address_ = static_cast<uint16_t>(arguments[1] << 8) + arguments[0];
        return;
    case Relative:
        relative_ = static_cast<int8_t>(arguments[0]);
        return;
    }

    address_ = 0;
    std::cerr << "Unsupported addressing mode: " << mode_ << std::endl;
}

std::string Instruction::typeToStr() const
{
    switch (type_) {
    case ADC:
        return "ADC";
    case AND:
        return "AND";
    case ASL:
        return "ASL";
    case BCC:
        return "BCC";
    case BCS:
        return "BCS";
    case BEQ:
        return "BEQ";
    case BIT:
        return "BIT";
    case BMI:
        return "BMI";
    case BNE:
        return "BNE";
    case BPL:
        return "BPL";
    case BRK:
        return "BRK";
    case BVC:
        return "BVC";
    case BVS:
        return "BVS";
    case CLC:
        return "CLC";
    case CLD:
        return "CLD";
    case CLI:
        return "CLI";
    case CLV:
        return "CLV";
    case CMP:
        return "CMP";
    case CPX:
        return "CPX";
    case CPY:
        return "CPY";
    case DEC:
        return "DEC";
    case DEX:
        return "DEX";
    case DEY:
        return "DEY";
    case EOR:
        return "EOR";
    case INC:
        return "INC";
    case INX:
        return "INX";
    case INY:
        return "INY";
    case JMP:
        return "JMP";
    case JSR:
        return "JSR";
    case LDA:
        return "LDA";
    case LDX:
        return "LDX";
    case LDY:
        return "LDY";
    case LSR:
        return "LSR";
    case NOP:
        return "NOP";
    case ORA:
        return "ORA";
    case PHA:
        return "PHA";
    case PHP:
        return "PHP";
    case PLA:
        return "PLA";
    case PLP:
        return "PLP";
    case ROL:
        return "ROL";
    case ROR:
        return "ROR";
    case RTI:
        return "RTI";
    case RTS:
        return "RTS";
    case SBC:
        return "SBC";
    case SEC:
        return "SEC";
    case SED:
        return "SED";
    case SEI:
        return "SEI";
    case STA:
        return "STA";
    case STX:
        return "STX";
    case STY:
        return "STY";
    case TAX:
        return "TAX";
    case TAY:
        return "TAY";
    case TSX:
        return "TSX";
    case TXA:
        return "TXA";
    case TXS:
        return "TXS";
    case TYA:
        return "TYA";
    }

    return "UNKNOWN";
}


std::ostream & operator<<(std::ostream & out, const Instruction & instruction)
{
    out << instruction.typeToStr();

    /* FIXME: causes the following with -fsanitize=undefined:
       "ios_base.h:82:67: runtime error: load of value 4294967221, which is
       not a valid value for type 'std::_Ios_Fmtflags'" */
    out << std::hex << std::showbase;

    switch(instruction.mode_) {
    case Instruction::Relative:
        out << std::resetiosflags(std::ios_base::basefield)
            << std::resetiosflags(std::ios_base::showbase);

        out << ", " << static_cast<int>(instruction.relative_);

        break;
    case Instruction::IndirectX:
        out << ", (" << static_cast<unsigned>(instruction.immediate_);
        out << ",x)";

        break;
    case Instruction::IndirectY:
        out << ", (" << static_cast<unsigned>(instruction.immediate_);
        out << "),y";

        break;
    case Instruction::Immediate:
        out << ", #" << static_cast<unsigned>(instruction.immediate_);

        break;
    case Instruction::ZeroPage:
        out << ", " << static_cast<unsigned>(instruction.immediate_);

        break;
    case Instruction::Implied:
        break;
    case Instruction::ZeroPageX:
        out << ", " << static_cast<unsigned>(instruction.immediate_) << ",x";

        break;
    case Instruction::AbsoluteX:
        out << ", " << instruction.address_ << ",x";

        break;
    case Instruction::AbsoluteY:
        out << ", " << instruction.address_ << ",y";

        break;
    case Instruction::Absolute:
        out << ", " << instruction.address_;

        break;
    case Instruction::Indirect:
        out << ", (" << instruction.address_ << ")";

        break;
    }

    out << std::resetiosflags(std::ios_base::basefield)
        << std::resetiosflags(std::ios_base::showbase);

    return out;
}
