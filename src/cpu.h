#pragma once

#include <cinttypes>
#include <vector>
#include <array>
#include <stack>
#include <fstream>
#include <map>

class PPU;
class INesHeader;
class Instruction;
class Controllers;

class CPU {
public:
    CPU(PPU & ppu, Controllers & controllers);
    CPU(const INesHeader & header, std::ifstream & file, PPU & ppu,
        Controllers & controllers);

    void reset();
    void NMI();

    bool step();

    const uint8_t * getCHRStart() const;
    size_t getCHRSize() const;

    struct CPUState {
        bool carry;
        bool zero;
        bool interruptDisabled;
        bool decimalMode;
        bool breakCommand;
        bool overflow;
        bool negative;

        uint16_t programCounter;

        /* 256-byte stack between 0x0100 and 0x01FF, used with stackOffset. */
        uint8_t stackPointer;

        /* Registers */
        uint8_t a;
        uint8_t x;
        uint8_t y;

        friend std::ostream & operator<<(std::ostream & out, const CPUState & state);
    } state_;

    std::stack<CPUState> stack_;

    unsigned int prgSize_;
    unsigned int chrSize_;

    std::vector<uint8_t> rom_;
    std::array<uint8_t, 2048> ram_;

    uint8_t controllerPortStatus_;

    PPU & ppu_;
    Controllers & controllers_;

    bool interrupted_;

    static const size_t stackOffset = 0x0100;
    static const uint8_t stackTop = 0xFF;
    static const size_t prgBytes = 16384;
    static const size_t chrBytes = 8192;
    static const uint16_t DMAPort = 0x4014;
    static const uint16_t controllerPort1 = 0x4016;
    static const uint16_t controllerPort2 = 0x4017;

    static const std::map<uint8_t, Instruction> instructionSet;

private:
    bool loadToRegister(const Instruction & instruction, uint8_t * value);
    bool writeToMemory(const Instruction & instruction, uint8_t value);
    bool readFromMemory(const Instruction & instruction, uint8_t * value);
    bool doDMA(uint8_t startPos);
    void doAdd(uint8_t arg);
    void doCompare(const Instruction & instruction, uint8_t value);

    struct ProcessorStatus {
        uint8_t carry : 1,
            zero : 1,
            interrupt : 1,
            decimal : 1,
            breakFlag : 1,
            unused : 1,
            overflow : 1,
            negative : 1;
    };

    uint8_t stateToRegister() const;
    void setRegisterToState(uint8_t reg);

    void pushStack(uint8_t value);
    uint8_t popStack();

    Instruction getNextInstruction();

    uint16_t nmiVector_;
    uint16_t resetVector_;
    uint16_t irqVector_;
};
