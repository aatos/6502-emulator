#pragma once

#include <cinttypes>
#include <vector>
#include <array>
#include <stack>
#include <fstream>
#include <map>

class PPU;
class INesHeader;
class Instruction;
class Controllers;

class CPU {
public:
    CPU(PPU & ppu, Controllers & controllers);

    bool loadROM(const INesHeader & header, std::ifstream & file);

    void reset();
    void NMI();

    bool step();

    const uint8_t * getCHRStart() const;
    size_t getCHRSize() const;

    static const size_t kStackOffset = 0x0100;
    static const uint8_t kStackTop = 0xFF;
    static const size_t kPRGBytes = 16384;
    static const size_t kCHRBytes = 8192;
    static const uint16_t kDMAPort = 0x4014;
    static const uint16_t kControllerPort1 = 0x4016;
    static const uint16_t kControllerPort2 = 0x4017;
    static const size_t kPRGStart = 0x8000;
    static const size_t kPRGEnd = 0xFFFF;

private:
    bool loadArgument(const Instruction & instruction, uint8_t * value);
    bool writeToMemory(const Instruction & instruction, uint8_t value);
    bool readFromMemory(const Instruction & instruction, uint8_t * value);

    bool doDMA(uint8_t startPos);
    void doAdd(uint8_t arg);
    void doCompare(const Instruction & instruction, uint8_t value);
    void doConditionalJump(const Instruction & instruction, bool condition);

    void updateZeroAndNegative(uint8_t value);

    uint16_t readIndirectAddress(uint16_t address) const;
    uint16_t addressFromROM(uint16_t romAddress) const;

    uint8_t stateToRegister() const;
    void setRegisterToState(uint8_t reg);

    void pushStack(uint8_t value);
    uint8_t popStack();

    Instruction getNextInstruction();

    struct CPUState {
        bool carry;
        bool zero;
        bool interruptDisabled;
        bool decimalMode;
        bool breakCommand;
        bool overflow;
        bool negative;

        uint16_t programCounter;

        /* 256-byte stack between 0x0100 and 0x01FF, used with stackOffset. */
        uint8_t stackPointer;

        /* Registers */
        uint8_t a;
        uint8_t x;
        uint8_t y;
    } state_;

    std::stack<CPUState> stack_;

    uint16_t prgSize_;
    unsigned int chrSize_; // TODO: not sure how much CHR there can be

    std::vector<uint8_t> rom_;
    std::array<uint8_t, 2048> ram_;

    uint8_t controllerPortStatus_;

    PPU & ppu_;
    Controllers & controllers_;

    bool interrupted_;

    uint16_t nmiVector_;
    uint16_t resetVector_;
    uint16_t irqVector_;

    static const std::map<uint8_t, Instruction> kInstructionSet;

    friend std::ostream & operator<<(std::ostream & out, const CPU & cpu);

    friend class CPUTest;
};
