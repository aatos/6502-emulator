#pragma once

#include <cinttypes>

class APU {
public:
    static bool isAPUAddress(uint16_t address);
};
