#pragma once

#include <cinttypes>
#include <string>

/* Instructions and their modes for 6502 processor. Reference from
 * http://www.obelisk.demon.co.uk/6502/reference.html. */
class Instruction {
public:
    enum AddressingModes {
        Implied,
        Immediate,
        ZeroPage,
        ZeroPageX,
        Absolute,
        AbsoluteX,
        AbsoluteY,
        Indirect,
        IndirectX,
        IndirectY,
        Relative
    };

    enum InstructionTypes {
        ADC,
        AND,
        ASL,
        BCC,
        BCS,
        BEQ,
        BIT,
        BMI,
        BNE,
        BPL,
        BRK,
        BVC,
        BVS,
        CLC,
        CLD,
        CLI,
        CLV,
        CMP,
        CPX,
        CPY,
        DEC,
        DEX,
        DEY,
        EOR,
        INC,
        INX,
        INY,
        JMP,
        JSR,
        LDA,
        LDX,
        LDY,
        LSR,
        NOP,
        ORA,
        PHA,
        PHP,
        PLA,
        PLP,
        ROL,
        ROR,
        RTI,
        RTS,
        SBC,
        SEC,
        SED,
        SEI,
        STA,
        STX,
        STY,
        TAX,
        TAY,
        TSX,
        TXA,
        TXS,
        TYA
    };

    Instruction(InstructionTypes type, AddressingModes mode);

    uint8_t getArgumentsLength() const;
    void setArguments(const uint8_t * arguments);

    std::string typeToStr() const;

    InstructionTypes type_;
    union {
        uint16_t address_;
        uint8_t immediate_;
        int8_t relative_;
    };
    AddressingModes mode_;

    friend std::ostream & operator<<(std::ostream & out, const Instruction & instruction);
};
