#pragma once

#include <ctime>

class Timer {
public:
    Timer(void (*sighandler)(int), long interval);

private:
    timer_t timer_;
    long interval_; // Nanoseconds
};
