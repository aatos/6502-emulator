#include "timer.h"

#include <cstring> // For std::strerror
#include <csignal>
#include <iostream>

Timer::Timer(void (*sighandler)(int), long interval) : interval_(interval)
{
    struct sigaction sa = {};
    sa.sa_flags = SA_RESTART; // Is restart ok?
    sa.sa_handler = sighandler;
    sigemptyset(&sa.sa_mask);

    if (sigaction(SIGRTMIN, &sa, NULL)) {
        std::cerr << "sigaction failed: " << std::strerror(errno) << std::endl;
        throw std::runtime_error("sigaction failed");
    }

    struct sigevent sev = {};
    sev.sigev_notify = SIGEV_SIGNAL;
    sev.sigev_signo = SIGRTMIN;

    if (timer_create(CLOCK_MONOTONIC, &sev, &timer_)) {
        std::cerr << "timer_create failed: " << std::strerror(errno) << std::endl;
        throw std::runtime_error("timer_create failed");
    }

    struct timespec ts = { 0, interval_ };
    struct itimerspec timespec = { ts, ts };
    if (timer_settime(timer_, 0, &timespec, NULL)) {
        std::cerr << "timer_settime failed: " << std::strerror(errno) << std::endl;
        throw std::runtime_error("timer_settime failed");
    }
}
