#include "apu.h"

bool APU::isAPUAddress(uint16_t address)
{
    return (address >= 0x4000 && address <= 0x4013) || address == 0x4015;
}
