#include "cpu.h"

#include <iostream>
#include <iomanip>
#include <stdexcept>

#include "ppu.h"
#include "ines.h"
#include "controllers.h"
#include "instruction.h"
#include "apu.h"

namespace {
    struct ProcessorStatus {
        uint8_t carry : 1,
            zero : 1,
            interrupt : 1,
            decimal : 1,
            breakFlag : 1,
            unused : 1,
            overflow : 1,
            negative : 1;
    };

    bool isValidPRGAddress(uint16_t address)
    {
        return address >= CPU::kPRGStart;
    }

    uint16_t getZeroPageAddress(const Instruction & instruction, uint8_t value)
    {
        // ZeroPage wraps around if it overflows
        return static_cast<uint8_t>(instruction.immediate_ + value);
    }

    uint16_t getAbsoluteAddress(const Instruction & instruction, uint8_t value)
    {
        return instruction.address_ + value;
    }
}

CPU::CPU(PPU & ppu, Controllers & controllers) :
    state_(), stack_(), prgSize_(0),
    chrSize_(0), rom_(), ram_(),
    controllerPortStatus_(0), ppu_(ppu),
    controllers_(controllers),
    interrupted_(false), nmiVector_(0),
    resetVector_(0), irqVector_(0)
{
    state_.stackPointer = kStackTop;
}

bool CPU::loadROM(const INesHeader & header, std::ifstream & file)
{
    std::streamoff afterHeader = file.tellg();
    file.seekg(0, file.end);

    std::streamoff fileEnd = file.tellg();
    file.seekg(afterHeader, file.beg);

    size_t romSize = static_cast<size_t>(fileEnd - afterHeader);

    rom_.resize(romSize);

    file.read(reinterpret_cast<char *>(rom_.data()), static_cast<std::streamoff>(romSize));

    if (!file) {
        std::cerr << "Couldn't read the ROM: eof: " << file.eof()
                  << ", fail: " << file.fail()
                  << ", bad: " << file.bad()
                  << std::endl;
        return false;
    }

    if (header.chr_size_ == 0 || header.prg_size_ == 0) {
        std::cerr << "CHR or PRG size cannot be zero!" << std::endl;
        return false;
    }

    chrSize_ = header.chr_size_ * kCHRBytes;
    prgSize_ = header.prg_size_ * kPRGBytes;

    if (chrSize_ + prgSize_ > rom_.size()) {
        std::cerr << "ROM size too small!" << std::endl;
        return false;
    }

    irqVector_ = static_cast<uint16_t>((rom_[prgSize_ - 1] << 8) | rom_[prgSize_ - 2]);
    resetVector_ = static_cast<uint16_t>((rom_[prgSize_ - 3] << 8) | rom_[prgSize_ - 4]);
    nmiVector_ = static_cast<uint16_t>((rom_[prgSize_ - 5] << 8) | rom_[prgSize_ - 6]);

    if (!isValidPRGAddress(resetVector_)) {
        std::cerr << "Invalid reset vector: " << resetVector_ << std::endl;
        return false;
    }

    // IRQ and NMI vectors are optional

    reset();

    return true;
}

void CPU::reset()
{
    state_ = CPUState();

    state_.stackPointer = kStackTop;
    state_.programCounter = addressFromROM(resetVector_);
}

void CPU::NMI()
{
    if (!interrupted_) {
        stack_.push(state_);

        state_.programCounter = addressFromROM(nmiVector_);
        interrupted_ = true;
    }
}

bool CPU::step()
{
#ifdef DEBUG_INSTR
    std::cout << *this <<  "   ";
#endif

    Instruction instruction = getNextInstruction();

#ifdef DEBUG_INSTR
    std::cout << instruction << std::endl;
#endif

    switch(instruction.type_) {
    case Instruction::PHA:
    {
        pushStack(state_.a);

        break;
    }
    case Instruction::PLA:
    {
        updateZeroAndNegative(state_.a = popStack());

        break;
    }
    case Instruction::PHP:
    {
        pushStack(stateToRegister());

        break;
    }
    case Instruction::PLP:
    {
        uint8_t reg = popStack();

        setRegisterToState(reg);

        break;
    }
    case Instruction::RTI:
    {
        if (stack_.size() != 1) {
            std::cerr << "Stack size is not 1 and RTI is called!" << std::endl;
            return false;
        }

        state_ = stack_.top();
        stack_.pop();

        interrupted_ = false;
        break;
    }
    case Instruction::RTS:
    {
        // Low byte first
        state_.programCounter = popStack();
        state_.programCounter |= (popStack() << 8);

        break;
    }
    case Instruction::SEI:
    {
        state_.interruptDisabled = true;
        break;
    }
    case Instruction::STA:
    {
        if (!writeToMemory(instruction, state_.a)) {
            return false;
        }
        break;
    }
    case Instruction::STX:
    {
        if (!writeToMemory(instruction, state_.x)) {
            return false;
        }
        break;
    }
    case Instruction::STY:
    {
        if (!writeToMemory(instruction, state_.y)) {
            return false;
        }
        break;
    }
    case Instruction::ADC:
    {
        uint8_t arg;
        if (!loadArgument(instruction, &arg)) {
            return false;
        }

        doAdd(arg);
        break;
    }
    case Instruction::AND:
    {
        uint8_t arg;
        if (!loadArgument(instruction, &arg)) {
            return false;
        }

        updateZeroAndNegative(state_.a &= arg);

        break;
    }
    case Instruction::BIT:
    {
        uint8_t arg;
        if (!readFromMemory(instruction, &arg)) {
            return false;
        }

        state_.overflow = (arg & 0x40);
        state_.negative = (arg & 0x80);
        state_.zero = !(arg & state_.a);
        break;
    }
    case Instruction::BCC:
    {
        doConditionalJump(instruction, !state_.carry);
        break;
    }
    case Instruction::BCS:
    {
        doConditionalJump(instruction, state_.carry);
        break;
    }
    case Instruction::BEQ:
    {
        doConditionalJump(instruction, state_.zero);
        break;
    }
    case Instruction::BNE:
    {
        doConditionalJump(instruction, !state_.zero);
        break;
    }
    case Instruction::BMI:
    {
        doConditionalJump(instruction, state_.negative);
        break;
    }
    case Instruction::BPL:
    {
        doConditionalJump(instruction, !state_.negative);
        break;
    }
    case Instruction::BVC:
    {
        doConditionalJump(instruction, !state_.overflow);
        break;
    }
    case Instruction::BVS:
    {
        doConditionalJump(instruction, state_.overflow);
        break;
    }
    case Instruction::DEC:
    {
        uint8_t arg;
        if (!readFromMemory(instruction, &arg)) {
            return false;
        }

        updateZeroAndNegative(--arg);

        if (!writeToMemory(instruction, arg)) {
            return false;
        }

        break;
    }
    case Instruction::DEX:
    {
        updateZeroAndNegative(--state_.x);
        break;
    }
    case Instruction::DEY:
    {
        updateZeroAndNegative(--state_.y);
        break;
    }
    case Instruction::EOR:
    {
        uint8_t arg;
        if (!loadArgument(instruction, &arg)) {
            return false;
        }

        updateZeroAndNegative(state_.a ^= arg);
        break;
    }
    case Instruction::CLC:
    {
        state_.carry = false;
        break;
    }
    case Instruction::CLI:
    {
        state_.interruptDisabled = false;
        break;
    }
    case Instruction::CLV:
    {
        state_.overflow = false;
        break;
    }
    case Instruction::SBC:
    {
        uint8_t arg;
        if (!loadArgument(instruction, &arg)) {
            return false;
        }

        /* Take one's complement of arg and SBC becomes ADC. As explained in
         * http://www.righto.com/2012/12/the-6502-overflow-flag-explained.html */
        doAdd(~arg);
        break;
    }
    case Instruction::SEC:
    {
        state_.carry = true;
        break;
    }
    case Instruction::SED:
    {
        state_.decimalMode = true;
        break;
    }
    case Instruction::CLD:
    {
        state_.decimalMode = false;
        break;
    }
    case Instruction::CPX:
    {
        doCompare(instruction, state_.x);
        break;
    }
    case Instruction::CPY:
    {
        doCompare(instruction, state_.y);
        break;
    }
    case Instruction::CMP:
    {
        doCompare(instruction, state_.a);
        break;
    }
    case Instruction::LDA:
    {
        if (!loadArgument(instruction, &state_.a)) {
            return false;
        }
        break;
    }
    case Instruction::LDX:
    {
        if (!loadArgument(instruction, &state_.x)) {
            return false;
        }
        break;
    }
    case Instruction::LDY:
    {
        if (!loadArgument(instruction, &state_.y)) {
            return false;
        }
        break;
    }
    case Instruction::ASL:
    {
        uint8_t arg;
        if (!loadArgument(instruction, &arg)) {
            return false;
        }

        state_.carry = arg & 0x80;

        arg <<= 1;

        updateZeroAndNegative(arg);

        switch (instruction.mode_) {
        case Instruction::Accumulator:
            state_.a = arg;
            break;
        default:
            if (!writeToMemory(instruction, arg)) {
                return false;
            }
            break;
        }

        break;
    }
    case Instruction::LSR:
    {
        uint8_t arg;
        if (!loadArgument(instruction, &arg)) {
            return false;
        }

        state_.carry = arg & 1;

        arg >>= 1;

        state_.zero = (arg == 0);
        state_.negative = false; // Can't be anything else than false

        switch (instruction.mode_) {
        case Instruction::Accumulator:
            state_.a = arg;
            break;
        default:
            if (!writeToMemory(instruction, arg)) {
                return false;
            }
            break;
        }

        break;
    }
    case Instruction::ROL:
    {
        uint8_t arg;
        if (!loadArgument(instruction, &arg)) {
            return false;
        }

        uint8_t oldCarry = (state_.carry ? 1 : 0);

        state_.carry = arg & 0x80;

        arg <<= 1;
        arg |= oldCarry;

        updateZeroAndNegative(arg);

        switch (instruction.mode_) {
        case Instruction::Accumulator:
            state_.a = arg;
            break;
        default:
            if (!writeToMemory(instruction, arg)) {
                return false;
            }
            break;
        }

        break;
    }
    case Instruction::ROR:
    {
        uint8_t arg;
        if (!loadArgument(instruction, &arg)) {
            return false;
        }

        uint8_t oldCarry = (state_.carry ? 0x80 : 0);

        state_.carry = arg & 0x1;

        arg >>= 1;
        arg |= oldCarry;

        updateZeroAndNegative(arg);

        switch (instruction.mode_) {
        case Instruction::Accumulator:
            state_.a = arg;
            break;
        default:
            if (!writeToMemory(instruction, arg)) {
                return false;
            }
            break;
        }

        break;
    }
    case Instruction::NOP:
    {
        break;
    }
    case Instruction::ORA:
    {
        uint8_t arg;
        if (!loadArgument(instruction, &arg)) {
            return false;
        }

        updateZeroAndNegative(state_.a |= arg);

        break;
    }
    case Instruction::TAX:
    {
        updateZeroAndNegative(state_.x = state_.a);
        break;
    }
    case Instruction::TXS:
    {
        state_.stackPointer = state_.x;
        break;
    }
    case Instruction::TSX:
    {
        updateZeroAndNegative(state_.x = state_.stackPointer);
        break;
    }
    case Instruction::TXA:
    {
        updateZeroAndNegative(state_.a = state_.x);
        break;
    }
    case Instruction::TYA:
    {
        updateZeroAndNegative(state_.a = state_.y);
        break;
    }
    case Instruction::TAY:
    {
        updateZeroAndNegative(state_.y = state_.a);
        break;
    }
    case Instruction::INC:
    {
        uint8_t arg;
        if (!readFromMemory(instruction, &arg)) {
            return false;
        }

        updateZeroAndNegative(++arg);

        if (!writeToMemory(instruction, arg)) {
            return false;
        }

        break;
    }
    case Instruction::INX:
    {
        updateZeroAndNegative(++state_.x);
        break;
    }
    case Instruction::INY:
    {
        updateZeroAndNegative(++state_.y);
        break;
    }
    case Instruction::JMP:
    {
        uint16_t arg;

        switch(instruction.mode_) {
        case Instruction::Absolute:
            arg = instruction.address_;
            break;
        case Instruction::Indirect:
            arg = readIndirectAddress(instruction.address_);
            break;
        default:
            std::cerr << "Unsupported addressing mode: " << instruction << std::endl;
            return false;
        }

        state_.programCounter = addressFromROM(arg);
        break;
    }
    case Instruction::JSR:
    {
        uint16_t arg;

        switch(instruction.mode_) {
        case Instruction::Absolute:
            arg = instruction.address_;
            break;
        default:
            std::cerr << "Unsupported addressing mode: " << instruction << std::endl;
            return false;
        }

        // High byte first
        pushStack(state_.programCounter >> 8);
        pushStack(state_.programCounter & 0xFF);

        state_.programCounter = addressFromROM(arg);
        break;
    }
    case Instruction::BRK:
    {
        std::cerr << "BRK is not supported" << std::endl;
        return false;
    }
    default:
        std::cerr << "Illegal instruction: " << instruction << std::endl;
        return false;
    }


    return true;
}


const uint8_t * CPU::getCHRStart() const
{
    // CHR starts after PRG
    return rom_.data() + prgSize_;
}

size_t CPU::getCHRSize() const
{
    return rom_.size() - prgSize_;
}

bool CPU::loadArgument(const Instruction & instruction, uint8_t * arg)
{
    switch(instruction.mode_) {
    case Instruction::Immediate:
        *arg = instruction.immediate_;
        break;
    case Instruction::Accumulator:
        *arg = state_.a;
        break;
    case Instruction::IndirectX:
    case Instruction::IndirectY:
    case Instruction::ZeroPage:
    case Instruction::ZeroPageX:
    case Instruction::ZeroPageY:
    case Instruction::Absolute:
    case Instruction::AbsoluteX:
    case Instruction::AbsoluteY:
        if (!readFromMemory(instruction, arg)) {
            return false;
        }
        break;
    default:
        std::cerr << "Unsupported addressing mode: " << instruction << std::endl;
        return false;
    }

    updateZeroAndNegative(*arg);

    return true;
}

bool CPU::writeToMemory(const Instruction & instruction, uint8_t value)
{
    uint16_t dest;

    switch (instruction.mode_) {
    case Instruction::ZeroPage:
        dest = instruction.immediate_;
        break;
    case Instruction::ZeroPageX:
        dest = getZeroPageAddress(instruction, state_.x);
        break;
    case Instruction::ZeroPageY:
        dest = getZeroPageAddress(instruction, state_.y);
        break;
    case Instruction::IndirectX:
        dest = readIndirectAddress(static_cast<uint8_t>(instruction.immediate_ + state_.x));
        break;
    case Instruction::IndirectY:
        dest = readIndirectAddress(instruction.immediate_) + state_.y;
        break;
    case Instruction::Absolute:
        dest = instruction.address_;
        break;
    case Instruction::AbsoluteX:
        dest = getAbsoluteAddress(instruction, state_.x);
        break;
    case Instruction::AbsoluteY:
        dest = getAbsoluteAddress(instruction, state_.y);
        break;
    default:
        std::cerr << "Unsupported addressing mode: " << instruction << std::endl;
        return false;
    }

    if (dest < ram_.size()) {
        ram_[dest] = value;
    } else if (PPU::isPPUAddress(dest)) {
        ppu_.write(dest, value);
    } else if (APU::isAPUAddress(dest)) {
        // APU is not supported
    } else if (dest == kDMAPort) {
        return doDMA(value);
    } else if (dest == kControllerPort1 || dest == kControllerPort2) {
        // Latch controllers if 1 and 0 is written to controller port 1
        // TODO: or is it odd and even? http://wiki.nesdev.com/w/index.php/Controller_Reading
        if (controllerPortStatus_ == 0x1 && value == 0x0) {
            controllers_.latch();
        }

        controllerPortStatus_ = value;
    } else {
        std::cerr << "Unsupported address: " << dest
                  << ", instruction: " << instruction << std::endl;
        return false;
    }

    return true;
}

bool CPU::readFromMemory(const Instruction & instruction, uint8_t * value)
{
    uint16_t src;

    switch(instruction.mode_) {
    case Instruction::ZeroPage:
        src = instruction.immediate_;
        break;
    case Instruction::ZeroPageX:
        src = getZeroPageAddress(instruction, state_.x);
        break;
    case Instruction::ZeroPageY:
        src = getZeroPageAddress(instruction, state_.y);
        break;
    case Instruction::IndirectX:
        src = readIndirectAddress(static_cast<uint8_t>(instruction.immediate_ + state_.x));
        break;
    case Instruction::IndirectY:
        src = readIndirectAddress(instruction.immediate_) + state_.y;
        break;
    case Instruction::Absolute:
        src = instruction.address_;
        break;
    case Instruction::AbsoluteX:
        src = getAbsoluteAddress(instruction, state_.x);
        break;
    case Instruction::AbsoluteY:
        src = getAbsoluteAddress(instruction, state_.y);
        break;
    default:
        std::cerr << "Unsupported addressing mode: "
                  << instruction << std::endl;
        return false;
    }

    // http://wiki.nesdev.com/w/index.php/CPU_memorymap
    // Mirroring is not supported

    if (src < ram_.size()) {
        *value = ram_[src];
    } else if (PPU::isPPUAddress(src)) {
        *value = ppu_.read(src);
    } else if (APU::isAPUAddress(src)) {
        // APU is not supported
        *value = 0;
    } else if (isValidPRGAddress(src)) { // ROM
        *value = rom_.at(addressFromROM(src));
    } else if (src == kControllerPort1) {
        *value = controllers_.read(Controllers::ControllerNumber::PlayerOne);
    } else if (src == kControllerPort2) {
        *value = controllers_.read(Controllers::ControllerNumber::PlayerTwo);
    } else {
        std::cerr << "Unsupported address: " << src
                  << ", instruction: " << instruction << std::endl;
        return false;
    }

    return true;
}

bool CPU::doDMA(uint8_t startPos)
{
    const uint16_t startAddress = static_cast<uint16_t>(startPos << 8);
    const size_t size = ppu_.oamData_.size();

    if (startAddress >= ram_.size() || startAddress + size >= ram_.size()) {
        std::cerr << "Invalid DMA start address: " << startAddress << std::endl;
        return false;
    }

    for (uint16_t i = 0; i < size; ++i) {
        ppu_.oamData_[(ppu_.oamAddress_ + i) % size] = ram_[startAddress + i];
    }

    return true;
}

void CPU::doAdd(uint8_t arg)
{
    uint8_t res = state_.a + arg + (state_.carry ? 1 : 0);

    state_.carry = (res - arg != state_.a + (state_.carry ? 1 : 0));
    /* Explained nicely in
     * http://www.righto.com/2012/12/the-6502-overflow-flag-explained.html */
    state_.overflow = ((arg ^ res) & (state_.a ^ res) & 0x80) != 0;

    updateZeroAndNegative(res);

    state_.a = res;
}

void CPU::doCompare(const Instruction & instruction, uint8_t value)
{
    uint8_t arg;
    switch(instruction.mode_) {
    case Instruction::Immediate:
        arg = instruction.immediate_;
        break;
    default:
        if (!readFromMemory(instruction, &arg)) {
            throw std::invalid_argument("Invalid instruction!");
        }
        break;
    }

    state_.carry = (value >= arg);
    state_.zero = (value == arg);
    state_.negative = (value < arg);
}

void CPU::doConditionalJump(const Instruction & instruction, bool condition)
{
    if (condition) {
        state_.programCounter += instruction.relative_;
    }
}

void CPU::updateZeroAndNegative(uint8_t value)
{
    state_.zero = (value == 0);
    state_.negative = (value & 0x80);
}

/* Transforms current processor status to uint8_t that can be pushed to stack
 * with PHP. Reference from http://nesdev.com/6502_cpu.txt. */
uint8_t CPU::stateToRegister() const
{
    ProcessorStatus status;
    status.carry = state_.carry ? 1 : 0;
    status.zero = state_.zero ? 1 : 0;
    status.interrupt = state_.interruptDisabled ? 1 : 0;
    status.decimal = state_.decimalMode ? 1 : 0;
    status.breakFlag = state_.breakCommand ? 1 : 0;
    status.unused = 1;
    status.overflow = state_.overflow ? 1 : 0;
    status.negative = state_.negative ? 1 : 0;

    return *reinterpret_cast<uint8_t *>(&status);
}

void CPU::setRegisterToState(uint8_t reg)
{
    ProcessorStatus status = *reinterpret_cast<ProcessorStatus *>(&reg);

    state_.carry = status.carry;
    state_.zero = status.zero;
    state_.interruptDisabled = status.interrupt;
    state_.decimalMode = status.decimal;
    state_.breakCommand = status.breakFlag;
    state_.overflow = status.overflow;
    state_.negative = status.negative;
}

void CPU::pushStack(uint8_t value)
{
    ram_[kStackOffset + state_.stackPointer] = value;

    --state_.stackPointer;
}

uint8_t CPU::popStack()
{
    if (state_.stackPointer == kStackTop) {
        throw std::invalid_argument("Stack is empty!");
    }

    ++state_.stackPointer;
    return ram_[kStackOffset + state_.stackPointer];
}

// FIXME: get rid of this
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wglobal-constructors"
#endif
const std::map<uint8_t, Instruction> CPU::kInstructionSet = {
    { 0x69, Instruction(Instruction::ADC, Instruction::Immediate) },
    { 0x65, Instruction(Instruction::ADC, Instruction::ZeroPage) },
    { 0x75, Instruction(Instruction::ADC, Instruction::ZeroPageX) },
    { 0x6D, Instruction(Instruction::ADC, Instruction::Absolute) },
    { 0x7D, Instruction(Instruction::ADC, Instruction::AbsoluteX) },
    { 0x79, Instruction(Instruction::ADC, Instruction::AbsoluteY) },
    { 0x61, Instruction(Instruction::ADC, Instruction::IndirectX) },
    { 0x71, Instruction(Instruction::ADC, Instruction::IndirectY) },

    { 0x29, Instruction(Instruction::AND, Instruction::Immediate) },
    { 0x25, Instruction(Instruction::AND, Instruction::ZeroPage) },
    { 0x35, Instruction(Instruction::AND, Instruction::ZeroPageX) },
    { 0x2D, Instruction(Instruction::AND, Instruction::Absolute) },
    { 0x3D, Instruction(Instruction::AND, Instruction::AbsoluteX) },
    { 0x39, Instruction(Instruction::AND, Instruction::AbsoluteY) },
    { 0x21, Instruction(Instruction::AND, Instruction::IndirectX) },
    { 0x31, Instruction(Instruction::AND, Instruction::IndirectY) },

    { 0x0A, Instruction(Instruction::ASL, Instruction::Accumulator) },
    { 0x06, Instruction(Instruction::ASL, Instruction::ZeroPage) },
    { 0x16, Instruction(Instruction::ASL, Instruction::ZeroPageX) },
    { 0x0E, Instruction(Instruction::ASL, Instruction::Absolute) },
    { 0x1E, Instruction(Instruction::ASL, Instruction::AbsoluteX) },

    { 0x24, Instruction(Instruction::BIT, Instruction::ZeroPage) },
    { 0x2C, Instruction(Instruction::BIT, Instruction::Absolute) },

    { 0x90, Instruction(Instruction::BCC, Instruction::Relative) },
    { 0xB0, Instruction(Instruction::BCS, Instruction::Relative) },
    { 0xF0, Instruction(Instruction::BEQ, Instruction::Relative) },
    { 0xD0, Instruction(Instruction::BNE, Instruction::Relative) },
    { 0x30, Instruction(Instruction::BMI, Instruction::Relative) },
    { 0x10, Instruction(Instruction::BPL, Instruction::Relative) },
    { 0x50, Instruction(Instruction::BVC, Instruction::Relative) },
    { 0x70, Instruction(Instruction::BVS, Instruction::Relative) },

    { 0x00, Instruction(Instruction::BRK, Instruction::Implied) },

    { 0xD8, Instruction(Instruction::CLD, Instruction::Implied) },
    { 0x18, Instruction(Instruction::CLC, Instruction::Implied) },
    { 0x58, Instruction(Instruction::CLI, Instruction::Implied) },
    { 0xB8, Instruction(Instruction::CLV, Instruction::Implied) },

    { 0xC9, Instruction(Instruction::CMP, Instruction::Immediate) },
    { 0xC5, Instruction(Instruction::CMP, Instruction::ZeroPage) },
    { 0xD5, Instruction(Instruction::CMP, Instruction::ZeroPageX) },
    { 0xCD, Instruction(Instruction::CMP, Instruction::Absolute) },
    { 0xDD, Instruction(Instruction::CMP, Instruction::AbsoluteX) },
    { 0xD9, Instruction(Instruction::CMP, Instruction::AbsoluteY) },
    { 0xC1, Instruction(Instruction::CMP, Instruction::IndirectX) },
    { 0xD1, Instruction(Instruction::CMP, Instruction::IndirectY) },

    { 0xE0, Instruction(Instruction::CPX, Instruction::Immediate) },
    { 0xE4, Instruction(Instruction::CPX, Instruction::ZeroPage) },
    { 0xEC, Instruction(Instruction::CPX, Instruction::Absolute) },

    { 0xC0, Instruction(Instruction::CPY, Instruction::Immediate) },
    { 0xC4, Instruction(Instruction::CPY, Instruction::ZeroPage) },
    { 0xCC, Instruction(Instruction::CPY, Instruction::Absolute) },

    { 0xC6, Instruction(Instruction::DEC, Instruction::ZeroPage) },
    { 0xD6, Instruction(Instruction::DEC, Instruction::ZeroPageX) },
    { 0xCE, Instruction(Instruction::DEC, Instruction::Absolute) },
    { 0xDE, Instruction(Instruction::DEC, Instruction::AbsoluteX) },

    { 0xCA, Instruction(Instruction::DEX, Instruction::Implied) },
    { 0x88, Instruction(Instruction::DEY, Instruction::Implied) },

    { 0x49, Instruction(Instruction::EOR, Instruction::Immediate) },
    { 0x45, Instruction(Instruction::EOR, Instruction::ZeroPage) },
    { 0x55, Instruction(Instruction::EOR, Instruction::ZeroPageX) },
    { 0x4D, Instruction(Instruction::EOR, Instruction::Absolute) },
    { 0x5D, Instruction(Instruction::EOR, Instruction::AbsoluteX) },
    { 0x59, Instruction(Instruction::EOR, Instruction::AbsoluteY) },
    { 0x41, Instruction(Instruction::EOR, Instruction::IndirectX) },
    { 0x51, Instruction(Instruction::EOR, Instruction::IndirectY) },

    { 0xE6, Instruction(Instruction::INC, Instruction::ZeroPage) },
    { 0xF6, Instruction(Instruction::INC, Instruction::ZeroPageX) },
    { 0xEE, Instruction(Instruction::INC, Instruction::Absolute) },
    { 0xFE, Instruction(Instruction::INC, Instruction::AbsoluteX) },

    { 0xE8, Instruction(Instruction::INX, Instruction::Implied) },
    { 0xC8, Instruction(Instruction::INY, Instruction::Implied) },

    { 0x4C, Instruction(Instruction::JMP, Instruction::Absolute) },
    { 0x6C, Instruction(Instruction::JMP, Instruction::Indirect) },

    { 0x20, Instruction(Instruction::JSR, Instruction::Absolute) },

    { 0xA5, Instruction(Instruction::LDA, Instruction::ZeroPage) },
    { 0xB5, Instruction(Instruction::LDA, Instruction::ZeroPageX) },
    { 0xA9, Instruction(Instruction::LDA, Instruction::Immediate) },
    { 0xAD, Instruction(Instruction::LDA, Instruction::Absolute) },
    { 0xBD, Instruction(Instruction::LDA, Instruction::AbsoluteX) },
    { 0xB9, Instruction(Instruction::LDA, Instruction::AbsoluteY) },
    { 0xA1, Instruction(Instruction::LDA, Instruction::IndirectX) },
    { 0xB1, Instruction(Instruction::LDA, Instruction::IndirectY) },

    { 0xA0, Instruction(Instruction::LDY, Instruction::Immediate) },
    { 0xA4, Instruction(Instruction::LDY, Instruction::ZeroPage) },
    { 0xB4, Instruction(Instruction::LDY, Instruction::ZeroPageX) },
    { 0xAC, Instruction(Instruction::LDY, Instruction::Absolute) },
    { 0xBC, Instruction(Instruction::LDY, Instruction::AbsoluteX) },

    { 0xA2, Instruction(Instruction::LDX, Instruction::Immediate) },
    { 0xA6, Instruction(Instruction::LDX, Instruction::ZeroPage) },
    { 0xB6, Instruction(Instruction::LDX, Instruction::ZeroPageY) },
    { 0xAE, Instruction(Instruction::LDX, Instruction::Absolute) },
    { 0xBE, Instruction(Instruction::LDX, Instruction::AbsoluteY) },

    { 0x4A, Instruction(Instruction::LSR, Instruction::Accumulator) },
    { 0x46, Instruction(Instruction::LSR, Instruction::ZeroPage) },
    { 0x56, Instruction(Instruction::LSR, Instruction::ZeroPageX) },
    { 0x4E, Instruction(Instruction::LSR, Instruction::Absolute) },
    { 0x5E, Instruction(Instruction::LSR, Instruction::AbsoluteX) },

    { 0xEA, Instruction(Instruction::NOP, Instruction::Implied) },

    { 0x09, Instruction(Instruction::ORA, Instruction::Immediate) },
    { 0x05, Instruction(Instruction::ORA, Instruction::ZeroPage) },
    { 0x15, Instruction(Instruction::ORA, Instruction::ZeroPageX) },
    { 0x0D, Instruction(Instruction::ORA, Instruction::Absolute) },
    { 0x1D, Instruction(Instruction::ORA, Instruction::AbsoluteX) },
    { 0x19, Instruction(Instruction::ORA, Instruction::AbsoluteY) },
    { 0x01, Instruction(Instruction::ORA, Instruction::IndirectX) },
    { 0x11, Instruction(Instruction::ORA, Instruction::IndirectY) },

    { 0x48, Instruction(Instruction::PHA, Instruction::Implied) },
    { 0x68, Instruction(Instruction::PLA, Instruction::Implied) },

    { 0x08, Instruction(Instruction::PHP, Instruction::Implied) },
    { 0x28, Instruction(Instruction::PLP, Instruction::Implied) },

    { 0x2A, Instruction(Instruction::ROL, Instruction::Accumulator) },
    { 0x26, Instruction(Instruction::ROL, Instruction::ZeroPage) },
    { 0x36, Instruction(Instruction::ROL, Instruction::ZeroPageX) },
    { 0x2E, Instruction(Instruction::ROL, Instruction::Absolute) },
    { 0x3E, Instruction(Instruction::ROL, Instruction::AbsoluteX) },

    { 0x6A, Instruction(Instruction::ROR, Instruction::Accumulator) },
    { 0x66, Instruction(Instruction::ROR, Instruction::ZeroPage) },
    { 0x76, Instruction(Instruction::ROR, Instruction::ZeroPageX) },
    { 0x6E, Instruction(Instruction::ROR, Instruction::Absolute) },
    { 0x7E, Instruction(Instruction::ROR, Instruction::AbsoluteX) },

    { 0x40, Instruction(Instruction::RTI, Instruction::Implied) },
    { 0x60, Instruction(Instruction::RTS, Instruction::Implied) },

    { 0xE9, Instruction(Instruction::SBC, Instruction::Immediate) },
    { 0xE5, Instruction(Instruction::SBC, Instruction::ZeroPage) },
    { 0xF5, Instruction(Instruction::SBC, Instruction::ZeroPageX) },
    { 0xED, Instruction(Instruction::SBC, Instruction::Absolute) },
    { 0xFD, Instruction(Instruction::SBC, Instruction::AbsoluteX) },
    { 0xF9, Instruction(Instruction::SBC, Instruction::AbsoluteY) },
    { 0xE1, Instruction(Instruction::SBC, Instruction::IndirectX) },
    { 0xF1, Instruction(Instruction::SBC, Instruction::IndirectY) },

    { 0x38, Instruction(Instruction::SEC, Instruction::Implied) },
    { 0xF8, Instruction(Instruction::SED, Instruction::Implied) },
    { 0x78, Instruction(Instruction::SEI, Instruction::Implied) },

    { 0x86, Instruction(Instruction::STX, Instruction::ZeroPage) },
    { 0x96, Instruction(Instruction::STX, Instruction::ZeroPageY) },
    { 0x8E, Instruction(Instruction::STX, Instruction::Absolute) },

    { 0x84, Instruction(Instruction::STY, Instruction::ZeroPage) },
    { 0x94, Instruction(Instruction::STY, Instruction::ZeroPageX) },
    { 0x8C, Instruction(Instruction::STY, Instruction::Absolute) },

    { 0x85, Instruction(Instruction::STA, Instruction::ZeroPage) },
    { 0x95, Instruction(Instruction::STA, Instruction::ZeroPageX) },
    { 0x8D, Instruction(Instruction::STA, Instruction::Absolute) },
    { 0x9D, Instruction(Instruction::STA, Instruction::AbsoluteX) },
    { 0x99, Instruction(Instruction::STA, Instruction::AbsoluteY) },
    { 0x81, Instruction(Instruction::STA, Instruction::IndirectX) },
    { 0x91, Instruction(Instruction::STA, Instruction::IndirectY) },

    { 0xAA, Instruction(Instruction::TAX, Instruction::Implied) },
    { 0x8A, Instruction(Instruction::TXA, Instruction::Implied) },
    { 0xBA, Instruction(Instruction::TSX, Instruction::Implied) },
    { 0x9A, Instruction(Instruction::TXS, Instruction::Implied) },
    { 0x98, Instruction(Instruction::TYA, Instruction::Implied) },
    { 0xA8, Instruction(Instruction::TAY, Instruction::Implied) }
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

Instruction CPU::getNextInstruction()
{
    if (state_.programCounter >= rom_.size()) {
        std::cerr << "Invalid PC: 0x" << std::hex
                  << state_.programCounter << ", when rom size: "
                  << rom_.size() << std::dec << std::endl;
        throw std::runtime_error("Invalid PC!");
    }

    uint8_t opcode = rom_[state_.programCounter];

    auto it = kInstructionSet.find(opcode);
    if (it == kInstructionSet.end()) {
        std::cerr << "Invalid instruction: 0x" << std::hex
                  << int(opcode) << std::dec << std::endl;
        throw std::invalid_argument("Invalid instruction!");
    }

    ++state_.programCounter;

    Instruction instruction = it->second;

    instruction.setArguments(rom_.data() + state_.programCounter);

    state_.programCounter += instruction.getArgumentsLength();

    return instruction;
}

uint16_t CPU::readIndirectAddress(uint16_t address) const
{
    return ram_[address] | ((ram_[address + 1] << 8) & 0xFF00);
}

uint16_t CPU::addressFromROM(uint16_t romAddress) const
{
    /* ROM resides in the memory from 0x8000 to 0xFFFF, but our addressing goes
       from 0 to 0x7FFF. In addition, the ROM itself can be either 16KB or 32KB,
       which grows downwards (i.e. 16KB ROM would have data in 0xC000 to
       0xFFFF). So when the ROM is accessed, we must take 0x8000 offset and the
       PRG size into account. */
    return prgSize_ - static_cast<uint16_t>((kPRGEnd + 1) - romAddress);
}

std::ostream & operator<<(std::ostream & out, const CPU & cpu)
{
    const CPU::CPUState & state = cpu.state_;

    out << std::hex << std::uppercase
        << "A:" << std::setw(2) << int(state.a) << " "
        << "X:" << std::setw(2) << int(state.x) << " "
        << "Y:" << std::setw(2) << int(state.y) << " "
        << "SP:" << std::setw(2) << int(state.stackPointer) << " "
        << "       " << std::setw(4) << state.programCounter;

    out << std::dec << std::nouppercase;

    return out;
}
