#include "windowSFML.h"

#include <stdexcept>
#include <iostream>

#include "controllers.h"
#include "ppu.h"

WindowSFML::WindowSFML(Controllers & controllers, unsigned int scale) :
    scale_(scale),
    window_(sf::VideoMode(scale * windowWidth_, scale * windowHeight_), "Emu",
            sf::Style::Titlebar | sf::Style::Close),
    sprites_(), isTexturesInitialized_(false), controllers_(controllers)
{
    window_.setVerticalSyncEnabled(true);
    // FIXME: consider moving to .Init()
    for (auto & nesSprite : sprites_) {
        if (!nesSprite.texture.create(textureWidth_, textureHeight_)) {
            throw std::runtime_error("Texture creation failed!");
        }
    }
}

bool WindowSFML::initTextures(const uint8_t * chrStart,
                              size_t chrSize)
{
    for (unsigned int t = 0; t < maxTextures_; ++t) {
        auto & nesSprite = sprites_[t];

        const uint8_t * chrTile = chrStart + t * (textureWidth_ + textureHeight_);
        if ((t + 1) * (textureWidth_ + textureHeight_) > chrSize) {
            throw std::runtime_error("CHR was too short!");
        }

        for (unsigned int i = 0; i < textureWidth_; ++i) {
            for (unsigned int j = 0; j < textureHeight_; ++j) {
                // FIXME: explain CHR format.. and fix the following abomination
                uint8_t pixelColor = (((chrTile[i + 8] & (0x80 >> (j % 8))) ? 2 : 0) |
                                        ((chrTile[i] & (0x80 >> (j % 8))) ? 1 : 0));
                unsigned int pixel = i * textureWidth_ + j;
                RGBAData *rgba = reinterpret_cast<RGBAData *>(nesSprite.pixels
                                                              + pixel * sizeof *rgba);
                switch(pixelColor) {
                case 3:
                    rgba->r = 0xff;
                    break;
                case 2:
                    rgba->g = 0xff;
                    break;
                case 1:
                    rgba->b = 0xff;
                    break;
                }

                rgba->a = 0xff;
                nesSprite.pixelColors[pixel] = pixelColor;
            }
        }

        nesSprite.texture.update(nesSprite.pixels);
        nesSprite.sprite.setTexture(nesSprite.texture);
        nesSprite.sprite.setScale(scale_, scale_);
    }

    isTexturesInitialized_ = true;

    return true;
}

bool WindowSFML::draw(const std::array<uint8_t, 256> & oamData,
                      const std::array<uint8_t, 16383> & vram,
                      const PPU::ControlStatus & attributes)
{
    if (window_.isOpen()) {
        sf::Event event;
        while (window_.pollEvent(event))
        {
            switch (event.type)
            {
            case sf::Event::Closed:
                window_.close();
                break;
            default:
                break;
            }
        }

        if (!isTexturesInitialized_) {
            return false;
        }

        window_.clear();

        /* First draw sprites with priority bit set (=behind background), then
         * background and then rest of the sprites. */

        drawSprites(oamData, vram,
                    attributes.spritePatternTableAddress,
                    true);
        drawBackground(vram,
                       attributes.backgroundPatternTableAddress);
        drawSprites(oamData, vram,
                    attributes.spritePatternTableAddress,
                    false);

        window_.display();

        return true;
    }

    return false;
}

void WindowSFML::drawSprites(const std::array<uint8_t, 256> & oamData,
                             const std::array<uint8_t, 16383> & vram,
                             bool secondPatternTable,
                             bool drawPriorities)
{
    uint16_t tileStart = (secondPatternTable ? PPU::secondPatternTableStart : 0);

    // FIXME: cast to different array?
    const PPU::OAMData *data = reinterpret_cast<const PPU::OAMData *>(oamData.data());
    for (size_t i = 0; i < oamData.size() / sizeof(PPU::OAMData); ++i) {
        // Skip zeroes
        if (data[i].tile == 0
            && data[i].y == 0 && data[i].x == 0
            && data[i].attrs == 0) {
            continue;
        }

        PPU::OAMDataAttribute attr =
            *reinterpret_cast<const PPU::OAMDataAttribute*>(&data[i].attrs);
        if ((drawPriorities && !attr.prio) || (!drawPriorities && attr.prio)) {
            continue;
        }

        uint16_t tile = tileStart + data[i].tile;
        if (tile >= maxTextures_) {
            std::cerr << "Invalid tile: " << int(tile) << std::endl;
            throw std::runtime_error("Invalid tile!");
        }

        auto & nesSprite = sprites_[tile];
        nesSprite.sprite.setPosition(sf::Vector2f(data[i].x * scale_, data[i].y * scale_));

        nesSprite.changeColor(PPU::spritePaletteAddresses, attr.palette, vram);

        nesSprite.flip(attr.verticalFlip, attr.horizontalFlip);

        window_.draw(nesSprite.sprite);
    }
}

void WindowSFML::drawBackground(const std::array<uint8_t, 16383> & vram,
                                bool secondPatternTable)
{
    // TODO: assuming nametable 1
    uint16_t nametableStart = PPU::nametableAddressStart;
    uint16_t tileStart = (secondPatternTable ? PPU::secondPatternTableStart : 0);

    uint16_t attributeStart = nametableStart + PPU::nametableSize;
    for (uint16_t i = 0; i < PPU::nametableSize; ++i) {
        uint16_t tile = tileStart + vram[nametableStart + i];
        if (tile >= maxTextures_) {
            std::cerr << "Invalid tile: " << int(tile) << std::endl;
            throw std::runtime_error("Invalid tile!");
        }

        auto & nesSprite = sprites_[tile];
        uint8_t textureIdxX = i % (windowWidth_ / textureWidth_);
        uint8_t textureIdxY = static_cast<uint8_t>(i / (windowWidth_ / textureWidth_));

        nesSprite.sprite.setPosition(sf::Vector2f(textureIdxX * textureWidth_ * scale_,
                                                  textureIdxY * textureHeight_ * scale_));

        uint8_t attribute = vram[attributeStart + PPU::getAttributeIdx(textureIdxX, textureIdxY)];
        uint8_t palette = (attribute >> PPU::getAttributeBits(textureIdxX, textureIdxY)) & 3;

        nesSprite.changeColor(PPU::backgroundPaletteAddresses, palette, vram);

        window_.draw(nesSprite.sprite);
    }
}

void WindowSFML::NESSprite::changeColor(const std::array<uint16_t, 4> & paletteAddresses,
                                        uint8_t palette,
                                        const std::array<uint8_t, 16383> & vram)
{
    const uint8_t palettes[4] = { vram[PPU::backgroundPaletteAddress],
                                  vram[paletteAddresses[palette]],
                                  vram[paletteAddresses[palette] + 1],
                                  vram[paletteAddresses[palette] + 2] };

    for (unsigned int pixel = 0;
         pixel < textureWidth_ * textureHeight_; ++pixel) {
        RGBAData *rgba = reinterpret_cast<RGBAData *>(pixels
                                                      + pixel * sizeof *rgba);
        uint8_t nesColor = palettes[pixelColors[pixel]];

        PPU::getRGBFromColor(nesColor, rgba->r, rgba->g, rgba->b);

        /* Set alpha to zero on background color to get rid of the black boxes
         * around sprites */
        if (pixelColors[pixel] == 0) {
            rgba->a = 0;
        }
    }

    texture.update(pixels);
}

void WindowSFML::NESSprite::flip(bool verticalFlip,
                                 bool horizontalFlip)
{
    int left = 0;
    int top = 0;
    int width = textureHeight_;
    int height = textureWidth_;

    if (horizontalFlip) {
        left = textureWidth_;
        width = static_cast<int>(-textureWidth_);
    }

    if (verticalFlip) {
        top = textureHeight_;
        height = static_cast<int>(-textureHeight_);
    }

    sprite.setTextureRect(sf::IntRect(left, top,
                                      width, height));
}

void WindowSFML::handleControls()
{
    if (!window_.hasFocus()) {
        return;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
        controllers_.press(Controllers::ControllerNumber::PlayerOne,
                           Controllers::Button::Up);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
        controllers_.press(Controllers::ControllerNumber::PlayerOne,
                           Controllers::Button::Down);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
        controllers_.press(Controllers::ControllerNumber::PlayerOne,
                           Controllers::Button::Left);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
        controllers_.press(Controllers::ControllerNumber::PlayerOne,
                           Controllers::Button::Right);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
        controllers_.press(Controllers::ControllerNumber::PlayerOne,
                           Controllers::Button::A);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
        controllers_.press(Controllers::ControllerNumber::PlayerOne,
                           Controllers::Button::B);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
        controllers_.press(Controllers::ControllerNumber::PlayerOne,
                           Controllers::Button::Start);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
        controllers_.press(Controllers::ControllerNumber::PlayerOne,
                           Controllers::Button::Select);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::K)) {
        controllers_.press(Controllers::ControllerNumber::PlayerTwo,
                           Controllers::Button::Up);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::J)) {
        controllers_.press(Controllers::ControllerNumber::PlayerTwo,
                           Controllers::Button::Down);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::H)) {
        controllers_.press(Controllers::ControllerNumber::PlayerTwo,
                           Controllers::Button::Left);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::L)) {
        controllers_.press(Controllers::ControllerNumber::PlayerTwo,
                           Controllers::Button::Right);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Y)) {
        controllers_.press(Controllers::ControllerNumber::PlayerTwo,
                           Controllers::Button::A);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::U)) {
        controllers_.press(Controllers::ControllerNumber::PlayerTwo,
                           Controllers::Button::B);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::I)) {
        controllers_.press(Controllers::ControllerNumber::PlayerTwo,
                           Controllers::Button::Start);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::O)) {
        controllers_.press(Controllers::ControllerNumber::PlayerTwo,
                           Controllers::Button::Select);
    }
}
