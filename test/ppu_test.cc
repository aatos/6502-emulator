#include "ppu.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "cpu.h"
#include "windowBase.h"
#include "windowMock.h"

class PPUTest : public ::testing::Test {
protected:
    PPUTest() : window(), ppu(window) {
    }

    virtual ~PPUTest() {
    }

    WindowMock window;
    PPU ppu;
};

TEST_F(PPUTest, SetVRAMAddress)
{
    EXPECT_TRUE(PPU::isPPUAddress(0x2002));
    EXPECT_TRUE(PPU::isPPUAddress(0x2006));
    ppu.addressHigh_ = false;

    // Check that reading $2002 resets address latch
    ppu.read(0x2002);
    EXPECT_TRUE(ppu.addressHigh_);

    ppu.write(0x2006, 0x3F);
    ppu.write(0x2006, 0x11);
    EXPECT_EQ(0x3F11, ppu.vramAddress_);

    ppu.write(0x2006, 0x01);
    ppu.write(0x2006, 0x20);
    EXPECT_EQ(0x0120, ppu.vramAddress_);

    // Check that only high address can be changed
    ppu.write(0x2006, 0xFF);
    EXPECT_EQ(0xFF20, ppu.vramAddress_);
    ppu.read(0x2002);
    EXPECT_TRUE(ppu.addressHigh_);
    ppu.write(0x2006, 0x11);
    EXPECT_FALSE(ppu.addressHigh_);
    EXPECT_EQ(0x1120, ppu.vramAddress_);
}

TEST_F(PPUTest, writeVRAM)
{
    EXPECT_TRUE(PPU::isPPUAddress(0x2002));
    EXPECT_TRUE(PPU::isPPUAddress(0x2006));
    EXPECT_TRUE(PPU::isPPUAddress(0x2007));

    ppu.read(0x2002);
    EXPECT_TRUE(ppu.addressHigh_);

    ppu.write(0x2006, 0x3F);
    ppu.write(0x2006, 0x10);
    EXPECT_EQ(0x3F10, ppu.vramAddress_);

    ppu.write(0x2007, 0xF);
    EXPECT_EQ(0xF, ppu.vram_.at(0x3F10));

    ppu.write(0x2007, 0x3);
    EXPECT_EQ(0x3, ppu.vram_.at(0x3F11));

    ppu.write(0x2006, 0x3F);
    ppu.write(0x2006, 0x10);
    EXPECT_EQ(0x3F10, ppu.vramAddress_);

    EXPECT_EQ(0xF, ppu.read(0x2007));
    EXPECT_EQ(0x3, ppu.read(0x2007));

    ppu.write(0x2006, 0x3F);
    ppu.write(0x2006, 0xFE);
    EXPECT_EQ(0x3FFE, ppu.vramAddress_);

    ppu.write(0x2007, 0x3);
    EXPECT_EQ(0x3, ppu.vram_.at(0x3FFE));

    EXPECT_EQ(0x0, ppu.vramAddress_);
}

TEST_F(PPUTest, oamData)
{
    EXPECT_TRUE(PPU::isPPUAddress(0x2003));

    ppu.write(0x2003, 0x3F);
    EXPECT_EQ(0x3F, ppu.read(0x2003));

    ppu.write(0x2003, 0x10);
    EXPECT_EQ(0x10, ppu.read(0x2003));

    ppu.write(0x2004, 0x22);
    EXPECT_EQ(0x11, ppu.read(0x2003));

    ppu.write(0x2004, 0xF5);
    EXPECT_EQ(0x12, ppu.read(0x2003));

    ppu.write(0x2003, 0x10);
    EXPECT_EQ(0x10, ppu.read(0x2003));
    EXPECT_EQ(0x22, ppu.read(0x2004));
    EXPECT_EQ(0x10, ppu.read(0x2003));

    ppu.write(0x2003, 0x11);
    EXPECT_EQ(0x11, ppu.read(0x2003));
    EXPECT_EQ(0xF5, ppu.read(0x2004));
    EXPECT_EQ(0x11, ppu.read(0x2003));
}

TEST_F(PPUTest, getAttribute)
{
    EXPECT_EQ(0, PPU::getAttributeIdx(0, 0));
    EXPECT_EQ(0, PPU::getAttributeIdx(2, 0));
    EXPECT_EQ(1, PPU::getAttributeIdx(4, 0));
    EXPECT_EQ(7, PPU::getAttributeIdx(31, 0));

    EXPECT_EQ(0, PPU::getAttributeIdx(0, 3));
    EXPECT_EQ(0, PPU::getAttributeIdx(2, 3));
    EXPECT_EQ(1, PPU::getAttributeIdx(4, 3));
    EXPECT_EQ(7, PPU::getAttributeIdx(31, 3));

    EXPECT_EQ(8, PPU::getAttributeIdx(0, 4));
    EXPECT_EQ(8, PPU::getAttributeIdx(2, 4));
    EXPECT_EQ(9, PPU::getAttributeIdx(4, 4));
    EXPECT_EQ(15, PPU::getAttributeIdx(31, 7));

    EXPECT_EQ(0, PPU::getAttributeBits(0, 0));
    EXPECT_EQ(0, PPU::getAttributeBits(1, 0));
    EXPECT_EQ(2, PPU::getAttributeBits(2, 0));
    EXPECT_EQ(2, PPU::getAttributeBits(31, 0));

    EXPECT_EQ(0, PPU::getAttributeBits(0, 1));
    EXPECT_EQ(0, PPU::getAttributeBits(1, 1));
    EXPECT_EQ(2, PPU::getAttributeBits(2, 1));
    EXPECT_EQ(2, PPU::getAttributeBits(31, 1));

    EXPECT_EQ(4, PPU::getAttributeBits(0, 2));
    EXPECT_EQ(4, PPU::getAttributeBits(1, 2));
    EXPECT_EQ(6, PPU::getAttributeBits(2, 3));
    EXPECT_EQ(6, PPU::getAttributeBits(31, 3));

    EXPECT_EQ(0, PPU::getAttributeBits(0, 4));
    EXPECT_EQ(0, PPU::getAttributeBits(1, 4));
    EXPECT_EQ(2, PPU::getAttributeBits(2, 5));
    EXPECT_EQ(6, PPU::getAttributeBits(31, 7));
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
