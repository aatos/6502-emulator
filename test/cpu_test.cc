#include "cpu.h"

#include <string>
#include <fstream>
#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "ppu.h"
#include "controllers.h"
#include "instruction.h"
#include "windowMock.h"

using ::testing::_;
using ::testing::NotNull;
using ::testing::Return;
using ::testing::SetArgPointee;

class CPUTest : public ::testing::Test {
protected:
    CPUTest() : controllers(), window(), ppu(window), cpu(ppu, controllers) {
    }

    virtual ~CPUTest() {
    }

    void doInstruction(Instruction::InstructionTypes type,
                       Instruction::AddressingModes mode, uint16_t args) {
        instructionExecute(type, mode, args);

        EXPECT_TRUE(cpu.step());
    }

    void doInstructionExpectFailure(Instruction::InstructionTypes type,
                                    Instruction::AddressingModes mode, uint16_t args) {
        instructionExecute(type, mode, args);

        EXPECT_FALSE(cpu.step());
    }

    Controllers controllers;
    WindowMock window;
    PPU ppu;
    CPU cpu;

private:
    void instructionExecute(Instruction::InstructionTypes type,
                            Instruction::AddressingModes mode, uint16_t args) {
        Instruction inst = Instruction(type, mode);
        if (args > 0) {
            inst.setArguments(reinterpret_cast<uint8_t *>(&args));
        }

        auto pair = std::find_if(CPU::instructionSet.begin(), CPU::instructionSet.end(),
                                 [&inst](auto mapPair) {
                                     return (mapPair.second.type_ == inst.type_
                                             && mapPair.second.mode_ == inst.mode_);
                                 });
        if (pair == CPU::instructionSet.end()) {
            FAIL() << "Tested instruction not found in the instruction set";
        }

        uint16_t romSize = cpu.state_.programCounter + inst.getArgumentsLength() + 1;

        if (cpu.rom_.size() < romSize) {
            cpu.rom_.resize(romSize);
        }

        cpu.rom_[cpu.state_.programCounter] = pair->first;

        memcpy(cpu.rom_.data() + cpu.state_.programCounter + 1,
               reinterpret_cast<uint8_t *>(&args), inst.getArgumentsLength());

    }

};

TEST_F(CPUTest, testLDX)
{
    doInstruction(Instruction::LDX, Instruction::Immediate, 0x20);

    EXPECT_EQ(0x20, cpu.state_.x);
}

TEST_F(CPUTest, testLDXBNE)
{
    doInstruction(Instruction::LDX, Instruction::Immediate, 0x00);

    EXPECT_EQ(0x0, cpu.state_.x);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_EQ(2, cpu.state_.programCounter);

    int16_t relative = -2;
    doInstruction(Instruction::BNE, Instruction::Relative, static_cast<uint16_t>(relative));

    EXPECT_EQ(4, cpu.state_.programCounter);
}

TEST_F(CPUTest, XShouldWrap)
{
    doInstruction(Instruction::LDX, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0xFF, cpu.state_.x);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_EQ(2, cpu.state_.programCounter);

    doInstruction(Instruction::INX, Instruction::Implied, 0);

    EXPECT_EQ(0x0, cpu.state_.x);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_EQ(3, cpu.state_.programCounter);
}

TEST_F(CPUTest, testCLC)
{
    cpu.state_.carry = true;
    doInstruction(Instruction::CLC, Instruction::Implied, 0);
    EXPECT_FALSE(cpu.state_.carry);

    doInstruction(Instruction::CLC, Instruction::Implied, 0);
    EXPECT_FALSE(cpu.state_.carry);
}

TEST_F(CPUTest, add)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x1, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_EQ(2, cpu.state_.programCounter);

    doInstruction(Instruction::CLC, Instruction::Implied, 0);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0xFE);

    EXPECT_EQ(0xFF, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_EQ(5, cpu.state_.programCounter);

    doInstruction(Instruction::ADC, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_EQ(7, cpu.state_.programCounter);
}

TEST_F(CPUTest, addWithCarry)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x1, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0xFD);

    EXPECT_EQ(0xFF, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_TRUE(cpu.state_.negative);
}

TEST_F(CPUTest, addWithCarryWithOverflow)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x1, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0x1, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0xFE);

    EXPECT_EQ(0xFE, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);

    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0xFE, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_TRUE(cpu.state_.negative);
}

TEST_F(CPUTest, testADCWithOverflow)
{
    // Adding 64 + 64 = 128 -> the sign should be wrong so overflow is set
    doInstruction(Instruction::CLC, Instruction::Implied, 0x0);
    EXPECT_FALSE(cpu.state_.carry);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x40);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0x40);

    EXPECT_EQ(0x80, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.carry);
    EXPECT_TRUE(cpu.state_.overflow);
    EXPECT_TRUE(cpu.state_.negative);

    // Adding positive and negative, overflow should not be set
    doInstruction(Instruction::CLC, Instruction::Implied, 0x0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x40);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0x80);

    EXPECT_EQ(0xC0, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_TRUE(cpu.state_.negative);

    /* Adding -128 and -128 (negative and negative) -> positive so overflow
       should be set */
    doInstruction(Instruction::CLC, Instruction::Implied, 0x0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0x80);

    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_TRUE(cpu.state_.overflow);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::CLC, Instruction::Implied, 0x0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::CLC, Instruction::Implied, 0x0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0x7F, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_TRUE(cpu.state_.overflow);
    EXPECT_FALSE(cpu.state_.negative);
}

TEST_F(CPUTest, testNMI)
{
    cpu.state_.a = 5;
    cpu.state_.programCounter = 5;

    cpu.NMI();

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x1, cpu.state_.a);

    doInstruction(Instruction::RTI, Instruction::Implied, 0);

    EXPECT_EQ(5, cpu.state_.programCounter);
    EXPECT_EQ(5, cpu.state_.a);
}

TEST_F(CPUTest, testDMA)
{
    for (uint16_t i = 0; i < 256; ++i) {
        cpu.ram_[0x0200 + i] = static_cast<uint8_t>(i);
    }

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x0);
    doInstruction(Instruction::STA, Instruction::Absolute, 0x2003);
    ASSERT_EQ(0x00, ppu.oamAddress_);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x2);
    doInstruction(Instruction::STA, Instruction::Absolute, 0x4014);

    for (uint16_t i = 0; i < 256; ++i) {
        EXPECT_EQ(i, ppu.oamData_[i]);
    }
}

TEST_F(CPUTest, testDMAfromEnd)
{

    const uint16_t startPos = 0xFE;

    for (uint16_t i = 0; i < 256; ++i) {
        cpu.ram_[0x100 + i] = static_cast<uint8_t>(i);
    }

    doInstruction(Instruction::LDA, Instruction::Immediate, startPos);
    doInstruction(Instruction::STA, Instruction::Absolute, 0x2003);
    ASSERT_EQ(startPos, ppu.oamAddress_);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);
    doInstruction(Instruction::STA, Instruction::Absolute, 0x4014);

    for (uint16_t i = 0; i < 256; ++i) {
        EXPECT_EQ(i, ppu.oamData_[(i + startPos) % 256]);
    }
}

TEST_F(CPUTest, testDMAInvalidAddress)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x9);
    doInstructionExpectFailure(Instruction::STA, Instruction::Absolute, 0x4014);
}

TEST_F(CPUTest, testAND)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x3);

    doInstruction(Instruction::AND, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x1, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);

    doInstruction(Instruction::AND, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0x80, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);

    doInstruction(Instruction::AND, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    cpu.ram_[0x02] = 0x1;

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x81);
    doInstruction(Instruction::AND, Instruction::Absolute, 0x2);

    EXPECT_EQ(0x1, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
}

TEST_F(CPUTest, testJSR)
{
    const uint16_t routineStart = 0xCfff;

    doInstruction(Instruction::JSR, Instruction::Absolute, routineStart);

    EXPECT_EQ(routineStart - 0xC000, // TODO
              cpu.state_.programCounter);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::RTS, Instruction::Implied, 0x0);

    EXPECT_EQ(0x80, cpu.state_.a);
    EXPECT_EQ(3, cpu.state_.programCounter);
}

TEST_F(CPUTest, testJMPIndirect)
{
    const uint16_t jumpAddress = 0xCFFE;
    const uint16_t indirectAddress  = 0x0234;

    cpu.ram_[indirectAddress] = jumpAddress & 0xFF;
    cpu.ram_[indirectAddress + 1] = (jumpAddress >> 8) & 0xFF;

    doInstruction(Instruction::JMP, Instruction::Indirect, indirectAddress);

    EXPECT_EQ(jumpAddress - 0xC000, // TODO
              cpu.state_.programCounter);
}

TEST_F(CPUTest, testSBC)
{
    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x5);
    doInstruction(Instruction::SBC, Instruction::Immediate, 0x5);

    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::CLC, Instruction::Implied, 0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x5);
    doInstruction(Instruction::SBC, Instruction::Immediate, 0x4);

    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::CLC, Instruction::Implied, 0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x5);
    doInstruction(Instruction::SBC, Instruction::Immediate, 0x5);

    EXPECT_EQ(0xFF, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_TRUE(cpu.state_.negative);

    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0xFF);
    doInstruction(Instruction::SBC, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::SBC, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x7F, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.overflow);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0xD0);
    doInstruction(Instruction::SBC, Instruction::Immediate, 0x70);

    EXPECT_EQ(0x60, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.overflow);
    EXPECT_FALSE(cpu.state_.negative);
}

TEST_F(CPUTest, testStack)
{
    EXPECT_EQ(0xFF, cpu.state_.stackPointer);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0xFF);
    EXPECT_EQ(0xFF, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.zero);

    doInstruction(Instruction::PHA, Instruction::Implied, 0);
    EXPECT_EQ(0xFF, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.zero);

    EXPECT_EQ(0xFE, cpu.state_.stackPointer);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);
    EXPECT_EQ(0x1, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.zero);

    doInstruction(Instruction::PLA, Instruction::Implied, 0);
    EXPECT_EQ(0xFF, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.zero);

    EXPECT_EQ(0xFF, cpu.state_.stackPointer);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x0);
    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_TRUE(cpu.state_.zero);

    doInstruction(Instruction::PHA, Instruction::Implied, 0);
    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_TRUE(cpu.state_.zero);

    EXPECT_EQ(0xFE, cpu.state_.stackPointer);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x5);
    EXPECT_EQ(0x5, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.zero);

    doInstruction(Instruction::PLA, Instruction::Implied, 0);
    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_TRUE(cpu.state_.zero);

    EXPECT_EQ(0xFF, cpu.state_.stackPointer);
}

TEST_F(CPUTest, testLDAZeroPage)
{
    uint8_t address = 0xFF;
    uint8_t value = 0x8;

    cpu.ram_[address] = value;

    doInstruction(Instruction::LDA, Instruction::ZeroPage, 0xFF);
    EXPECT_EQ(value, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.zero);
}

TEST_F(CPUTest, testLDAIndirectX)
{
    uint8_t address = 0xBA;
    uint8_t x = 0x5;
    uint16_t finalAddress = 0x799;
    uint8_t value = 0x8;

    cpu.ram_[address + x] = finalAddress & 0xFF;
    cpu.ram_[address + x + 1] = (finalAddress >> 8) & 0xFF;

    cpu.ram_[finalAddress] = value;

    doInstruction(Instruction::LDX, Instruction::Immediate, x);
    doInstruction(Instruction::LDA, Instruction::IndirectX, address);
    EXPECT_EQ(value, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.zero);
}

TEST_F(CPUTest, testLDAIndirectY)
{
    uint8_t address = 0xBA;
    uint8_t y = 0x5;
    uint16_t finalAddress = 0x799;
    uint8_t value = 0x8;

    cpu.ram_[address] = finalAddress & 0xFF;
    cpu.ram_[address + 1] = (finalAddress >> 8) & 0xFF;

    cpu.ram_[finalAddress + y] = value;

    doInstruction(Instruction::LDY, Instruction::Immediate, y);
    doInstruction(Instruction::LDA, Instruction::IndirectY, address);
    EXPECT_EQ(value, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.zero);
}

TEST_F(CPUTest, testSTAIndirectX)
{
    uint8_t address = 0xBA;
    uint8_t x = 0x5;
    uint16_t finalAddress = 0x799;
    uint8_t value = 0x8;

    cpu.ram_[address + x] = finalAddress & 0xFF;
    cpu.ram_[address + x + 1] = (finalAddress >> 8) & 0xFF;

    doInstruction(Instruction::LDA, Instruction::Immediate, value);
    doInstruction(Instruction::LDX, Instruction::Immediate, x);
    doInstruction(Instruction::STA, Instruction::IndirectX, address);
    EXPECT_EQ(value, cpu.ram_[finalAddress]);
}

TEST_F(CPUTest, testSTAIndirectY)
{
    uint8_t address = 0xBA;
    uint8_t y = 0x5;
    uint16_t finalAddress = 0x799;
    uint8_t value = 0x8;

    cpu.ram_[address] = finalAddress & 0xFF;
    cpu.ram_[address + 1] = (finalAddress >> 8) & 0xFF;

    doInstruction(Instruction::LDA, Instruction::Immediate, value);
    doInstruction(Instruction::LDY, Instruction::Immediate, y);
    doInstruction(Instruction::STA, Instruction::IndirectY, address);
    EXPECT_EQ(value, cpu.ram_[finalAddress + y]);
}

TEST_F(CPUTest, testLSR)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0b1101);

    doInstruction(Instruction::LSR, Instruction::Implied, 0);

    EXPECT_EQ(0b110, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LSR, Instruction::Implied, 0);

    EXPECT_EQ(0b11, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LSR, Instruction::Implied, 0);

    EXPECT_EQ(0x1, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LSR, Instruction::Implied, 0);

    EXPECT_EQ(0, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
}

TEST_F(CPUTest, testORA)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0b1101);
    doInstruction(Instruction::ORA, Instruction::Immediate, 0b1110);

    EXPECT_EQ(0b1111, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x7F);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::ORA, Instruction::Immediate, 0x80);

    EXPECT_EQ(0xFF, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x0);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::ORA, Instruction::Immediate, 0x0);

    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
}

TEST_F(CPUTest, testEOR)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0b1101);
    doInstruction(Instruction::EOR, Instruction::Immediate, 0b1110);

    EXPECT_EQ(0b11, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    cpu.ram_[0x02FF] = 0x40;

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x7F);
    doInstruction(Instruction::EOR, Instruction::Absolute, 0x02FF);

    EXPECT_EQ(0x3F, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x7F);
    doInstruction(Instruction::LDX, Instruction::Immediate, 0xF);
    doInstruction(Instruction::EOR, Instruction::AbsoluteX, 0x02F0);

    EXPECT_EQ(0x3F, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x7F);
    doInstruction(Instruction::LDY, Instruction::Immediate, 0xF);
    doInstruction(Instruction::EOR, Instruction::AbsoluteY, 0x02F0);

    EXPECT_EQ(0x3F, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
}

TEST_F(CPUTest, testINY)
{
    doInstruction(Instruction::LDY, Instruction::Immediate, 0x1);
    doInstruction(Instruction::INY, Instruction::Implied, 0);

    EXPECT_EQ(0x2, cpu.state_.y);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LDY, Instruction::Immediate, 0x7F);
    doInstruction(Instruction::INY, Instruction::Implied, 0);

    EXPECT_EQ(0x80, cpu.state_.y);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);

    doInstruction(Instruction::LDY, Instruction::Immediate, 0xFF);
    doInstruction(Instruction::INY, Instruction::Implied, 0);

    EXPECT_EQ(0x0, cpu.state_.y);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
}

TEST_F(CPUTest, testROL)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x7F);
    doInstruction(Instruction::ROL, Instruction::Implied, 0);

    EXPECT_EQ(0xFE, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.carry);

    doInstruction(Instruction::ROL, Instruction::Implied, 0);

    EXPECT_EQ(0xFC, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_TRUE(cpu.state_.carry);

    doInstruction(Instruction::ROL, Instruction::Implied, 0);

    EXPECT_EQ(0xF9, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_TRUE(cpu.state_.carry);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x0);
    doInstruction(Instruction::ROL, Instruction::Implied, 0);

    EXPECT_EQ(0x1, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.carry);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::ROL, Instruction::Implied, 0);

    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_TRUE(cpu.state_.carry);

    cpu.ram_[0x02FF] = 0x5;

    doInstruction(Instruction::ROL, Instruction::Absolute, 0x02FF);

    EXPECT_EQ(0xB, cpu.ram_[0x02FF]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.carry);
}

TEST_F(CPUTest, testROR)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x2);
    doInstruction(Instruction::ROR, Instruction::Implied, 0);

    EXPECT_EQ(0x1, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.carry);

    doInstruction(Instruction::ROR, Instruction::Implied, 0);

    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_TRUE(cpu.state_.carry);

    doInstruction(Instruction::ROR, Instruction::Implied, 0);

    EXPECT_EQ(0x80, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.carry);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::ROR, Instruction::Implied, 0);

    EXPECT_EQ(0x40, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.carry);

    cpu.ram_[0x02FF] = 0x5;

    doInstruction(Instruction::ROR, Instruction::Absolute, 0x02FF);

    EXPECT_EQ(0x2, cpu.ram_[0x02FF]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_TRUE(cpu.state_.carry);
}

TEST_F(CPUTest, testASL)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x40);
    doInstruction(Instruction::ASL, Instruction::Implied, 0);

    EXPECT_EQ(0x80, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.carry);

    doInstruction(Instruction::ASL, Instruction::Implied, 0);

    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_TRUE(cpu.state_.carry);

    doInstruction(Instruction::ASL, Instruction::Implied, 0);

    EXPECT_EQ(0x0, cpu.state_.a);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.carry);

    cpu.ram_[0x00] = 0x1;

    doInstruction(Instruction::ASL, Instruction::ZeroPage, 0x00);

    EXPECT_EQ(0x2, cpu.ram_[0x00]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.carry);

    cpu.ram_[0x0200] = 0x40;

    doInstruction(Instruction::ASL, Instruction::Absolute, 0x0200);

    EXPECT_EQ(0x80, cpu.ram_[0x0200]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.carry);
}

TEST_F(CPUTest, testLDYZeroPageX)
{
    cpu.ram_[0x00] = 0x1;

    doInstruction(Instruction::LDY, Instruction::ZeroPageX, 0x0);

    EXPECT_EQ(0x1, cpu.state_.y);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    cpu.ram_[0x2F] = 0x2;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x20);
    doInstruction(Instruction::LDY, Instruction::ZeroPageX, 0xF);

    EXPECT_EQ(0x2, cpu.state_.y);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    cpu.ram_[0x7F] = 0x3;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0xFF);
    doInstruction(Instruction::LDY, Instruction::ZeroPageX, 0x80);

    EXPECT_EQ(0x3, cpu.state_.y);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
}

TEST_F(CPUTest, testINCZeroPageX)
{
    doInstruction(Instruction::INC, Instruction::ZeroPageX, 0x0);

    EXPECT_EQ(0x1, cpu.ram_[0x0]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    cpu.ram_[0x2F] = 0x2;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x20);
    doInstruction(Instruction::INC, Instruction::ZeroPageX, 0xF);

    EXPECT_EQ(0x3, cpu.ram_[0x2F]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    cpu.ram_[0x7F] = 0x3;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0xFF);
    doInstruction(Instruction::INC, Instruction::ZeroPageX, 0x80);

    EXPECT_EQ(0x4, cpu.ram_[0x7F]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    cpu.ram_[0xFF] = 0xFF;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x0F);
    doInstruction(Instruction::INC, Instruction::ZeroPageX, 0xF0);

    EXPECT_EQ(0x0, cpu.ram_[0xFF]);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
}

TEST_F(CPUTest, testINC)
{
    doInstruction(Instruction::INC, Instruction::Absolute, 0x0);

    EXPECT_EQ(0x1, cpu.ram_[0x0]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    cpu.ram_[0x2F] = 0x2;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x20);
    doInstruction(Instruction::INC, Instruction::ZeroPageX, 0xF);

    EXPECT_EQ(0x3, cpu.ram_[0x2F]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    cpu.ram_[0x80] = 0x3;

    doInstruction(Instruction::INC, Instruction::ZeroPage, 0x80);

    EXPECT_EQ(0x4, cpu.ram_[0x80]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    cpu.ram_[0x2FF] = 0xFF;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x0F);
    doInstruction(Instruction::INC, Instruction::AbsoluteX, 0x2F0);

    EXPECT_EQ(0x0, cpu.ram_[0x2FF]);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
}

TEST_F(CPUTest, testDEC)
{
    doInstruction(Instruction::DEC, Instruction::Absolute, 0x0);

    EXPECT_EQ(0xFF, cpu.ram_[0x0]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);

    cpu.ram_[0x2F] = 0x2;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x20);
    doInstruction(Instruction::DEC, Instruction::ZeroPageX, 0xF);

    EXPECT_EQ(0x1, cpu.ram_[0x2F]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    cpu.ram_[0x80] = 0x3;

    doInstruction(Instruction::DEC, Instruction::ZeroPage, 0x80);

    EXPECT_EQ(0x2, cpu.ram_[0x80]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    cpu.ram_[0x2FF] = 0xFF;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x0F);
    doInstruction(Instruction::DEC, Instruction::AbsoluteX, 0x2F0);

    EXPECT_EQ(0xFE, cpu.ram_[0x2FF]);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);
}

TEST_F(CPUTest, testTAX)
{
    doInstruction(Instruction::LDX, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x1, cpu.state_.x);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::TAX, Instruction::Implied, 0x0);

    EXPECT_EQ(0x0, cpu.state_.x);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0xFF);
    doInstruction(Instruction::TAX, Instruction::Implied, 0x0);

    EXPECT_EQ(0xFF, cpu.state_.x);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);
}

TEST_F(CPUTest, testCMP)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x5);
    doInstruction(Instruction::CMP, Instruction::Immediate, 0x5);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x5);
    doInstruction(Instruction::CMP, Instruction::Immediate, 0x3);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x3);
    doInstruction(Instruction::CMP, Instruction::Immediate, 0x5);
    EXPECT_FALSE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);
}

TEST_F(CPUTest, testCPX)
{
    doInstruction(Instruction::LDX, Instruction::Immediate, 0x5);
    doInstruction(Instruction::CPX, Instruction::Immediate, 0x5);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x5);
    doInstruction(Instruction::CPX, Instruction::Immediate, 0x3);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x3);
    doInstruction(Instruction::CPX, Instruction::Immediate, 0x5);
    EXPECT_FALSE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);
}

TEST_F(CPUTest, testCPY)
{
    doInstruction(Instruction::LDY, Instruction::Immediate, 0x5);
    doInstruction(Instruction::CPY, Instruction::Immediate, 0x5);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    cpu.ram_[0x2FF] = 0xFF;

    doInstruction(Instruction::LDY, Instruction::Immediate, 0xFE);
    doInstruction(Instruction::CPY, Instruction::Absolute, 0x2FF);
    EXPECT_FALSE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.negative);

    cpu.ram_[0x2F] = 0xFE;

    doInstruction(Instruction::LDY, Instruction::Immediate, 0xFF);
    doInstruction(Instruction::CPY, Instruction::ZeroPage, 0x2F);
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
}

TEST_F(CPUTest, testAbsoluteY)
{
    cpu.ram_[0x02FF] = 0x5;

    doInstruction(Instruction::LDY, Instruction::Immediate, 0xF);
    doInstruction(Instruction::LDX, Instruction::AbsoluteY, 0x02F0);

    EXPECT_EQ(0x5, cpu.state_.x);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x6);
    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::SBC, Instruction::AbsoluteY, 0x02F0);

    EXPECT_EQ(0x1, cpu.state_.a);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_TRUE(cpu.state_.carry);

    doInstruction(Instruction::STA, Instruction::AbsoluteY, 0x02F0);
    EXPECT_EQ(0x1, cpu.ram_[0x02FF]);
}

TEST_F(CPUTest, testSTYZeroPageX)
{
    doInstruction(Instruction::LDY, Instruction::Immediate, 0x5);
    doInstruction(Instruction::LDX, Instruction::Immediate, 0xF);
    doInstruction(Instruction::STY, Instruction::ZeroPageX, 0xF0);

    EXPECT_EQ(0x5, cpu.ram_[0xFF]);
}

TEST_F(CPUTest, testStackProcessorStatus)
{
    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::SEI, Instruction::Implied, 0);
    cpu.state_.decimalMode = true;
    cpu.state_.negative = true;

    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_TRUE(cpu.state_.interruptDisabled);
    EXPECT_TRUE(cpu.state_.decimalMode);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_FALSE(cpu.state_.breakCommand);

    doInstruction(Instruction::PHP, Instruction::Implied, 0);

    // PHP shouldn't change the status
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_TRUE(cpu.state_.interruptDisabled);
    EXPECT_TRUE(cpu.state_.decimalMode);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_FALSE(cpu.state_.breakCommand);

    // Change status so PLP can be noticed
    doInstruction(Instruction::CLC, Instruction::Implied, 0);
    doInstruction(Instruction::CLD, Instruction::Implied, 0);
    cpu.state_.interruptDisabled = false;
    cpu.state_.negative = false;
    cpu.state_.zero = true;
    cpu.state_.overflow = true;
    cpu.state_.breakCommand = true;

    EXPECT_FALSE(cpu.state_.carry);
    EXPECT_FALSE(cpu.state_.interruptDisabled);
    EXPECT_FALSE(cpu.state_.decimalMode);
    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.overflow);
    EXPECT_TRUE(cpu.state_.breakCommand);

    doInstruction(Instruction::PLP, Instruction::Implied, 0);

    // Status should be same as before PHP
    EXPECT_TRUE(cpu.state_.carry);
    EXPECT_TRUE(cpu.state_.interruptDisabled);
    EXPECT_TRUE(cpu.state_.decimalMode);
    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.overflow);
    EXPECT_FALSE(cpu.state_.breakCommand);
}

TEST_F(CPUTest, testBIT)
{
    cpu.ram_[0xFF] = 0x5;

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x4);

    // Check that state changes
    cpu.state_.zero = true;
    cpu.state_.negative = true;
    cpu.state_.overflow = true;

    doInstruction(Instruction::BIT, Instruction::ZeroPage, 0xFF);

    EXPECT_FALSE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.overflow);

    cpu.ram_[0x02FF] = 0x80;

    doInstruction(Instruction::BIT, Instruction::Absolute, 0x2FF);

    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_TRUE(cpu.state_.zero);
    EXPECT_FALSE(cpu.state_.overflow);

    cpu.ram_[0x02FF] = 0xc0;

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::BIT, Instruction::Absolute, 0x2FF);

    EXPECT_TRUE(cpu.state_.negative);
    EXPECT_FALSE(cpu.state_.zero);
    EXPECT_TRUE(cpu.state_.overflow);
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
