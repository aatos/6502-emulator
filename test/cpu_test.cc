#include "cpu.h"

#include <string>
#include <fstream>
#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "ppu.h"
#include "controllers.h"
#include "instruction.h"
#include "windowMock.h"

using ::testing::_;
using ::testing::NotNull;
using ::testing::Return;
using ::testing::SetArgPointee;

class CPUTest : public ::testing::Test {
protected:
    CPUTest() : controllers(), window(), ppu(window), cpu(ppu, controllers) {
    }

    virtual ~CPUTest() {
    }

    void doInstruction(Instruction::InstructionTypes type,
                       Instruction::AddressingModes mode, uint16_t args) {
        instructionExecute(type, mode, args);

        EXPECT_TRUE(cpu.step());
    }

    void doInstructionExpectFailure(Instruction::InstructionTypes type,
                                    Instruction::AddressingModes mode, uint16_t args) {
        instructionExecute(type, mode, args);

        EXPECT_FALSE(cpu.step());
    }

    void CPUStateShouldBe(bool expected) {
        if (expected) {
            EXPECT_TRUE(getCPUState().carry);
            EXPECT_TRUE(getCPUState().zero);
            EXPECT_TRUE(getCPUState().negative);
            EXPECT_TRUE(getCPUState().overflow);
        } else {
            EXPECT_FALSE(getCPUState().carry);
            EXPECT_FALSE(getCPUState().zero);
            EXPECT_FALSE(getCPUState().negative);
            EXPECT_FALSE(getCPUState().overflow);
        }
    }

    void setCPUStateTo(bool expected) {
        getCPUState().carry = expected;
        getCPUState().zero = expected;
        getCPUState().negative = expected;
        getCPUState().overflow = expected;
    }

    CPU::CPUState & getCPUState() {
        return cpu.state_;
    }

    std::array<uint8_t, 2048> & getCPURAM() {
        return cpu.ram_;
    }

    void setCPUPrgSize(uint16_t size) {
        cpu.prgSize_ = size;
    }

    uint16_t CPUAddressFromROM(uint16_t address) {
        return cpu.addressFromROM(address);
    }

    Controllers controllers;
    WindowMock window;
    PPU ppu;
    CPU cpu;

private:
    void instructionExecute(Instruction::InstructionTypes type,
                            Instruction::AddressingModes mode, uint16_t args) {
        Instruction inst = Instruction(type, mode);
        if (args > 0) {
            inst.setArguments(reinterpret_cast<uint8_t *>(&args));
        }

        auto pair = std::find_if(CPU::kInstructionSet.begin(), CPU::kInstructionSet.end(),
                                 [&inst](auto mapPair) {
                                     return (mapPair.second.type_ == inst.type_
                                             && mapPair.second.mode_ == inst.mode_);
                                 });
        if (pair == CPU::kInstructionSet.end()) {
            FAIL() << "Tested instruction not found in the instruction set";
        }

        uint16_t romSize = cpu.state_.programCounter + inst.getArgumentsLength() + 1;

        if (cpu.rom_.size() < romSize) {
            cpu.rom_.resize(romSize);
        }

        cpu.rom_[cpu.state_.programCounter] = pair->first;

        memcpy(cpu.rom_.data() + cpu.state_.programCounter + 1,
               reinterpret_cast<uint8_t *>(&args), inst.getArgumentsLength());

    }

};

TEST_F(CPUTest, testLDX)
{
    doInstruction(Instruction::LDX, Instruction::Immediate, 0x20);

    EXPECT_EQ(0x20, getCPUState().x);
}

TEST_F(CPUTest, testLDXBNE)
{
    doInstruction(Instruction::LDX, Instruction::Immediate, 0x00);

    EXPECT_EQ(0x0, getCPUState().x);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_EQ(2, getCPUState().programCounter);

    int16_t relative = -2;
    doInstruction(Instruction::BNE, Instruction::Relative, static_cast<uint16_t>(relative));

    EXPECT_EQ(4, getCPUState().programCounter);
}

TEST_F(CPUTest, testLDXZeroPageY)
{
    const uint8_t stored = 0x3;
    getCPURAM()[0x05] = stored;
    getCPUState().y = 0x2;

    doInstruction(Instruction::LDX, Instruction::ZeroPageY, 0x03);

    EXPECT_EQ(stored, getCPUState().x);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
}

TEST_F(CPUTest, XShouldWrap)
{
    doInstruction(Instruction::LDX, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0xFF, getCPUState().x);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_EQ(2, getCPUState().programCounter);

    doInstruction(Instruction::INX, Instruction::Implied, 0);

    EXPECT_EQ(0x0, getCPUState().x);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_EQ(3, getCPUState().programCounter);
}

TEST_F(CPUTest, testCLC)
{
    getCPUState().carry = true;
    doInstruction(Instruction::CLC, Instruction::Implied, 0);
    EXPECT_FALSE(getCPUState().carry);

    doInstruction(Instruction::CLC, Instruction::Implied, 0);
    EXPECT_FALSE(getCPUState().carry);
}

TEST_F(CPUTest, add)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x1, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_EQ(2, getCPUState().programCounter);

    doInstruction(Instruction::CLC, Instruction::Implied, 0);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0xFE);

    EXPECT_EQ(0xFF, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_EQ(5, getCPUState().programCounter);

    doInstruction(Instruction::ADC, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_EQ(7, getCPUState().programCounter);
}

TEST_F(CPUTest, addWithCarry)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x1, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0xFD);

    EXPECT_EQ(0xFF, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_TRUE(getCPUState().negative);
}

TEST_F(CPUTest, addWithCarryWithOverflow)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x1, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0x1, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0xFE);

    EXPECT_EQ(0xFE, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);

    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0xFE, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_TRUE(getCPUState().negative);
}

TEST_F(CPUTest, testADCWithOverflow)
{
    // Adding 64 + 64 = 128 -> the sign should be wrong so overflow is set
    doInstruction(Instruction::CLC, Instruction::Implied, 0x0);
    EXPECT_FALSE(getCPUState().carry);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x40);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0x40);

    EXPECT_EQ(0x80, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().carry);
    EXPECT_TRUE(getCPUState().overflow);
    EXPECT_TRUE(getCPUState().negative);

    // Adding positive and negative, overflow should not be set
    doInstruction(Instruction::CLC, Instruction::Implied, 0x0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x40);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0x80);

    EXPECT_EQ(0xC0, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_TRUE(getCPUState().negative);

    /* Adding -128 and -128 (negative and negative) -> positive so overflow
       should be set */
    doInstruction(Instruction::CLC, Instruction::Implied, 0x0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0x80);

    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_TRUE(getCPUState().overflow);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::CLC, Instruction::Implied, 0x0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::CLC, Instruction::Implied, 0x0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::ADC, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0x7F, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_TRUE(getCPUState().overflow);
    EXPECT_FALSE(getCPUState().negative);
}

TEST_F(CPUTest, testNMI)
{
    getCPUState().a = 5;
    getCPUState().programCounter = 5;

    cpu.NMI();

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x1, getCPUState().a);

    doInstruction(Instruction::RTI, Instruction::Implied, 0);

    EXPECT_EQ(5, getCPUState().programCounter);
    EXPECT_EQ(5, getCPUState().a);
}

TEST_F(CPUTest, testDMA)
{
    for (uint16_t i = 0; i < 256; ++i) {
        getCPURAM()[0x0200 + i] = static_cast<uint8_t>(i);
    }

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x0);
    doInstruction(Instruction::STA, Instruction::Absolute, 0x2003);
    ASSERT_EQ(0x00, ppu.oamAddress_);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x2);
    doInstruction(Instruction::STA, Instruction::Absolute, 0x4014);

    for (uint16_t i = 0; i < 256; ++i) {
        EXPECT_EQ(i, ppu.oamData_[i]);
    }
}

TEST_F(CPUTest, testDMAfromEnd)
{

    const uint16_t startPos = 0xFE;

    for (uint16_t i = 0; i < 256; ++i) {
        getCPURAM()[0x100 + i] = static_cast<uint8_t>(i);
    }

    doInstruction(Instruction::LDA, Instruction::Immediate, startPos);
    doInstruction(Instruction::STA, Instruction::Absolute, 0x2003);
    ASSERT_EQ(startPos, ppu.oamAddress_);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);
    doInstruction(Instruction::STA, Instruction::Absolute, 0x4014);

    for (uint16_t i = 0; i < 256; ++i) {
        EXPECT_EQ(i, ppu.oamData_[(i + startPos) % 256]);
    }
}

TEST_F(CPUTest, testDMAInvalidAddress)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x9);
    doInstructionExpectFailure(Instruction::STA, Instruction::Absolute, 0x4014);
}

TEST_F(CPUTest, testAND)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x3);

    doInstruction(Instruction::AND, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x1, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);

    doInstruction(Instruction::AND, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0x80, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);

    doInstruction(Instruction::AND, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    getCPURAM()[0x02] = 0x1;

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x81);
    doInstruction(Instruction::AND, Instruction::Absolute, 0x2);

    EXPECT_EQ(0x1, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
}

TEST_F(CPUTest, testANDAbsoluteY)
{
    getCPURAM()[0x44] = 0x1;
    getCPUState().y = 0x40;

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x81);
    doInstruction(Instruction::AND, Instruction::AbsoluteY, 0x4);

    EXPECT_EQ(0x1, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
}

TEST_F(CPUTest, testANDIndirectY)
{
    uint8_t address = 0xBA;
    uint8_t y = 0x5;
    uint16_t finalAddress = 0x799;
    uint8_t value = 0x81;

    getCPURAM()[address] = finalAddress & 0xFF;
    getCPURAM()[address + 1] = (finalAddress >> 8) & 0xFF;

    getCPURAM()[finalAddress + y] = value;

    doInstruction(Instruction::LDA, Instruction::Immediate, 0xFE);
    doInstruction(Instruction::LDY, Instruction::Immediate, y);
    doInstruction(Instruction::AND, Instruction::IndirectY, address);

    EXPECT_EQ(0x80, getCPUState().a);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);
}

TEST_F(CPUTest, testBRK)
{
    doInstructionExpectFailure(Instruction::BRK, Instruction::Implied, 0x0);
}

TEST_F(CPUTest, testJSR)
{
    const uint16_t routineStart = 0xCFFF;
    setCPUPrgSize(0x4000);

    doInstruction(Instruction::JSR, Instruction::Absolute, routineStart);

    EXPECT_EQ(CPUAddressFromROM(routineStart),
              getCPUState().programCounter);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::RTS, Instruction::Implied, 0x0);

    EXPECT_EQ(0x80, getCPUState().a);
    EXPECT_EQ(3, getCPUState().programCounter);
}

TEST_F(CPUTest, testJMPIndirect)
{
    const uint16_t jumpAddress = 0xCFFE;
    const uint16_t indirectAddress  = 0x0234;
    setCPUPrgSize(0x4000);

    getCPURAM()[indirectAddress] = jumpAddress & 0xFF;
    getCPURAM()[indirectAddress + 1] = (jumpAddress >> 8) & 0xFF;

    doInstruction(Instruction::JMP, Instruction::Indirect, indirectAddress);

    EXPECT_EQ(CPUAddressFromROM(jumpAddress),
              getCPUState().programCounter);
}

TEST_F(CPUTest, testSBC)
{
    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x5);
    doInstruction(Instruction::SBC, Instruction::Immediate, 0x5);

    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::CLC, Instruction::Implied, 0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x5);
    doInstruction(Instruction::SBC, Instruction::Immediate, 0x4);

    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::CLC, Instruction::Implied, 0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x5);
    doInstruction(Instruction::SBC, Instruction::Immediate, 0x5);

    EXPECT_EQ(0xFF, getCPUState().a);
    EXPECT_FALSE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_TRUE(getCPUState().negative);

    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0xFF);
    doInstruction(Instruction::SBC, Instruction::Immediate, 0xFF);

    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::SBC, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x7F, getCPUState().a);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().overflow);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::LDA, Instruction::Immediate, 0xD0);
    doInstruction(Instruction::SBC, Instruction::Immediate, 0x70);

    EXPECT_EQ(0x60, getCPUState().a);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().overflow);
    EXPECT_FALSE(getCPUState().negative);
}

TEST_F(CPUTest, testStack)
{
    EXPECT_EQ(0xFF, getCPUState().stackPointer);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0xFF);
    EXPECT_EQ(0xFF, getCPUState().a);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);

    doInstruction(Instruction::PHA, Instruction::Implied, 0);
    EXPECT_EQ(0xFF, getCPUState().a);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);

    EXPECT_EQ(0xFE, getCPUState().stackPointer);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x1);
    EXPECT_EQ(0x1, getCPUState().a);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);

    doInstruction(Instruction::PLA, Instruction::Implied, 0);
    EXPECT_EQ(0xFF, getCPUState().a);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);

    EXPECT_EQ(0xFF, getCPUState().stackPointer);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x0);
    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_TRUE(getCPUState().zero);

    doInstruction(Instruction::PHA, Instruction::Implied, 0);
    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_TRUE(getCPUState().zero);

    EXPECT_EQ(0xFE, getCPUState().stackPointer);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x5);
    EXPECT_EQ(0x5, getCPUState().a);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);

    doInstruction(Instruction::PLA, Instruction::Implied, 0);
    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_TRUE(getCPUState().zero);

    EXPECT_EQ(0xFF, getCPUState().stackPointer);
}

TEST_F(CPUTest, testLDAZeroPage)
{
    uint8_t address = 0xFF;
    uint8_t value = 0x8;

    getCPURAM()[address] = value;

    doInstruction(Instruction::LDA, Instruction::ZeroPage, 0xFF);
    EXPECT_EQ(value, getCPUState().a);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);
}

TEST_F(CPUTest, testLDAIndirectX)
{
    uint8_t address = 0xBA;
    uint8_t x = 0x5;
    uint16_t finalAddress = 0x799;
    uint8_t value = 0x8;

    getCPURAM()[address + x] = finalAddress & 0xFF;
    getCPURAM()[address + x + 1] = (finalAddress >> 8) & 0xFF;

    getCPURAM()[finalAddress] = value;

    doInstruction(Instruction::LDX, Instruction::Immediate, x);
    doInstruction(Instruction::LDA, Instruction::IndirectX, address);
    EXPECT_EQ(value, getCPUState().a);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);
}

TEST_F(CPUTest, testLDAIndirectXOverflow)
{
    uint8_t address = 0xFE;
    uint8_t x = 0x5;

    uint8_t realAddress = address + x;

    uint16_t finalAddress = 0x799;
    uint8_t value = 0x8;

    getCPURAM()[realAddress] = finalAddress & 0xFF;
    getCPURAM()[realAddress + 1] = (finalAddress >> 8) & 0xFF;

    getCPURAM()[finalAddress] = value;

    doInstruction(Instruction::LDX, Instruction::Immediate, x);
    doInstruction(Instruction::LDA, Instruction::IndirectX, address);
    EXPECT_EQ(value, getCPUState().a);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);
}

TEST_F(CPUTest, testLDAIndirectY)
{
    uint8_t address = 0xBA;
    uint8_t y = 0x5;
    uint16_t finalAddress = 0x799;
    uint8_t value = 0x8;

    getCPURAM()[address] = finalAddress & 0xFF;
    getCPURAM()[address + 1] = (finalAddress >> 8) & 0xFF;

    getCPURAM()[finalAddress + y] = value;

    doInstruction(Instruction::LDY, Instruction::Immediate, y);
    doInstruction(Instruction::LDA, Instruction::IndirectY, address);
    EXPECT_EQ(value, getCPUState().a);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);
}

TEST_F(CPUTest, testSTAIndirectX)
{
    uint8_t address = 0xBA;
    uint8_t x = 0x5;
    uint16_t finalAddress = 0x799;
    uint8_t value = 0x8;

    getCPURAM()[address + x] = finalAddress & 0xFF;
    getCPURAM()[address + x + 1] = (finalAddress >> 8) & 0xFF;

    doInstruction(Instruction::LDA, Instruction::Immediate, value);
    doInstruction(Instruction::LDX, Instruction::Immediate, x);
    doInstruction(Instruction::STA, Instruction::IndirectX, address);
    EXPECT_EQ(value, getCPURAM()[finalAddress]);
}

TEST_F(CPUTest, testSTAIndirectXOverflow)
{
    uint8_t address = 0xFE;
    uint8_t x = 0x5;

    uint8_t realAddress = address + x;

    uint16_t finalAddress = 0x799;
    uint8_t value = 0x8;

    getCPURAM()[realAddress] = finalAddress & 0xFF;
    getCPURAM()[realAddress + 1] = (finalAddress >> 8) & 0xFF;

    doInstruction(Instruction::LDA, Instruction::Immediate, value);
    doInstruction(Instruction::LDX, Instruction::Immediate, x);
    doInstruction(Instruction::STA, Instruction::IndirectX, address);
    EXPECT_EQ(value, getCPURAM()[finalAddress]);
}

TEST_F(CPUTest, testSTAIndirectY)
{
    uint8_t address = 0xBA;
    uint8_t y = 0x5;
    uint16_t finalAddress = 0x799;
    uint8_t value = 0x8;

    getCPURAM()[address] = finalAddress & 0xFF;
    getCPURAM()[address + 1] = (finalAddress >> 8) & 0xFF;

    doInstruction(Instruction::LDA, Instruction::Immediate, value);
    doInstruction(Instruction::LDY, Instruction::Immediate, y);
    doInstruction(Instruction::STA, Instruction::IndirectY, address);
    EXPECT_EQ(value, getCPURAM()[finalAddress + y]);
}

TEST_F(CPUTest, testLSR)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0b1101);

    doInstruction(Instruction::LSR, Instruction::Accumulator, 0);

    EXPECT_EQ(0b110, getCPUState().a);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LSR, Instruction::Accumulator, 0);

    EXPECT_EQ(0b11, getCPUState().a);
    EXPECT_FALSE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LSR, Instruction::Accumulator, 0);

    EXPECT_EQ(0x1, getCPUState().a);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LSR, Instruction::Accumulator, 0);

    EXPECT_EQ(0, getCPUState().a);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
}

TEST_F(CPUTest, testORA)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0b1101);
    doInstruction(Instruction::ORA, Instruction::Immediate, 0b1110);

    EXPECT_EQ(0b1111, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x7F);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::ORA, Instruction::Immediate, 0x80);

    EXPECT_EQ(0xFF, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x0);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::ORA, Instruction::Immediate, 0x0);

    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
}

TEST_F(CPUTest, testEOR)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0b1101);
    doInstruction(Instruction::EOR, Instruction::Immediate, 0b1110);

    EXPECT_EQ(0b11, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    getCPURAM()[0x02FF] = 0x40;

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x7F);
    doInstruction(Instruction::EOR, Instruction::Absolute, 0x02FF);

    EXPECT_EQ(0x3F, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x7F);
    doInstruction(Instruction::LDX, Instruction::Immediate, 0xF);
    doInstruction(Instruction::EOR, Instruction::AbsoluteX, 0x02F0);

    EXPECT_EQ(0x3F, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x7F);
    doInstruction(Instruction::LDY, Instruction::Immediate, 0xF);
    doInstruction(Instruction::EOR, Instruction::AbsoluteY, 0x02F0);

    EXPECT_EQ(0x3F, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
}

TEST_F(CPUTest, testINY)
{
    doInstruction(Instruction::LDY, Instruction::Immediate, 0x1);
    doInstruction(Instruction::INY, Instruction::Implied, 0);

    EXPECT_EQ(0x2, getCPUState().y);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LDY, Instruction::Immediate, 0x7F);
    doInstruction(Instruction::INY, Instruction::Implied, 0);

    EXPECT_EQ(0x80, getCPUState().y);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);

    doInstruction(Instruction::LDY, Instruction::Immediate, 0xFF);
    doInstruction(Instruction::INY, Instruction::Implied, 0);

    EXPECT_EQ(0x0, getCPUState().y);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
}

TEST_F(CPUTest, testROL)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x7F);
    doInstruction(Instruction::ROL, Instruction::Accumulator, 0);

    EXPECT_EQ(0xFE, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().carry);

    doInstruction(Instruction::ROL, Instruction::Accumulator, 0);

    EXPECT_EQ(0xFC, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_TRUE(getCPUState().carry);

    doInstruction(Instruction::ROL, Instruction::Accumulator, 0);

    EXPECT_EQ(0xF9, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_TRUE(getCPUState().carry);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x0);
    doInstruction(Instruction::ROL, Instruction::Accumulator, 0);

    EXPECT_EQ(0x1, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().carry);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::ROL, Instruction::Accumulator, 0);

    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_TRUE(getCPUState().carry);

    getCPURAM()[0x02FF] = 0x5;

    doInstruction(Instruction::ROL, Instruction::Absolute, 0x02FF);

    EXPECT_EQ(0xB, getCPURAM()[0x02FF]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().carry);
}

TEST_F(CPUTest, testROR)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x2);
    doInstruction(Instruction::ROR, Instruction::Accumulator, 0);

    EXPECT_EQ(0x1, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().carry);

    doInstruction(Instruction::ROR, Instruction::Accumulator, 0);

    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_TRUE(getCPUState().carry);

    doInstruction(Instruction::ROR, Instruction::Accumulator, 0);

    EXPECT_EQ(0x80, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().carry);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::ROR, Instruction::Accumulator, 0);

    EXPECT_EQ(0x40, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().carry);

    getCPURAM()[0x02FF] = 0x5;

    doInstruction(Instruction::ROR, Instruction::Absolute, 0x02FF);

    EXPECT_EQ(0x2, getCPURAM()[0x02FF]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_TRUE(getCPUState().carry);
}

TEST_F(CPUTest, testASL)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x40);
    doInstruction(Instruction::ASL, Instruction::Accumulator, 0);

    EXPECT_EQ(0x80, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().carry);

    doInstruction(Instruction::ASL, Instruction::Accumulator, 0);

    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_TRUE(getCPUState().carry);

    doInstruction(Instruction::ASL, Instruction::Accumulator, 0);

    EXPECT_EQ(0x0, getCPUState().a);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().carry);

    getCPURAM()[0x00] = 0x1;

    doInstruction(Instruction::ASL, Instruction::ZeroPage, 0x00);

    EXPECT_EQ(0x2, getCPURAM()[0x00]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().carry);

    getCPURAM()[0x0200] = 0x40;

    doInstruction(Instruction::ASL, Instruction::Absolute, 0x0200);

    EXPECT_EQ(0x80, getCPURAM()[0x0200]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().carry);
}

TEST_F(CPUTest, testLDYZeroPageX)
{
    getCPURAM()[0x00] = 0x1;

    doInstruction(Instruction::LDY, Instruction::ZeroPageX, 0x0);

    EXPECT_EQ(0x1, getCPUState().y);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    getCPURAM()[0x2F] = 0x2;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x20);
    doInstruction(Instruction::LDY, Instruction::ZeroPageX, 0xF);

    EXPECT_EQ(0x2, getCPUState().y);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    getCPURAM()[0x7F] = 0x3;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0xFF);
    doInstruction(Instruction::LDY, Instruction::ZeroPageX, 0x80);

    EXPECT_EQ(0x3, getCPUState().y);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
}

TEST_F(CPUTest, testSTXZeroPageY)
{
    getCPUState().x = 0x5;

    getCPUState().y = 0x0;
    setCPUStateTo(true);
    doInstruction(Instruction::STX, Instruction::ZeroPageY, 0x0);
    EXPECT_EQ(getCPURAM()[0x00], getCPUState().x);
    CPUStateShouldBe(true);

    getCPUState().y = 0x2;
    setCPUStateTo(false);
    doInstruction(Instruction::STX, Instruction::ZeroPageY, 0x3);
    EXPECT_EQ(getCPURAM()[0x05], getCPUState().x);
    CPUStateShouldBe(false);
}

TEST_F(CPUTest, testINCZeroPageX)
{
    doInstruction(Instruction::INC, Instruction::ZeroPageX, 0x0);

    EXPECT_EQ(0x1, getCPURAM()[0x0]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    getCPURAM()[0x2F] = 0x2;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x20);
    doInstruction(Instruction::INC, Instruction::ZeroPageX, 0xF);

    EXPECT_EQ(0x3, getCPURAM()[0x2F]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    getCPURAM()[0x7F] = 0x3;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0xFF);
    doInstruction(Instruction::INC, Instruction::ZeroPageX, 0x80);

    EXPECT_EQ(0x4, getCPURAM()[0x7F]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    getCPURAM()[0xFF] = 0xFF;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x0F);
    doInstruction(Instruction::INC, Instruction::ZeroPageX, 0xF0);

    EXPECT_EQ(0x0, getCPURAM()[0xFF]);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
}

TEST_F(CPUTest, testINC)
{
    doInstruction(Instruction::INC, Instruction::Absolute, 0x0);

    EXPECT_EQ(0x1, getCPURAM()[0x0]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    getCPURAM()[0x2F] = 0x2;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x20);
    doInstruction(Instruction::INC, Instruction::ZeroPageX, 0xF);

    EXPECT_EQ(0x3, getCPURAM()[0x2F]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    getCPURAM()[0x80] = 0x3;

    doInstruction(Instruction::INC, Instruction::ZeroPage, 0x80);

    EXPECT_EQ(0x4, getCPURAM()[0x80]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    getCPURAM()[0x2FF] = 0xFF;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x0F);
    doInstruction(Instruction::INC, Instruction::AbsoluteX, 0x2F0);

    EXPECT_EQ(0x0, getCPURAM()[0x2FF]);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
}

TEST_F(CPUTest, testDEC)
{
    doInstruction(Instruction::DEC, Instruction::Absolute, 0x0);

    EXPECT_EQ(0xFF, getCPURAM()[0x0]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);

    getCPURAM()[0x2F] = 0x2;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x20);
    doInstruction(Instruction::DEC, Instruction::ZeroPageX, 0xF);

    EXPECT_EQ(0x1, getCPURAM()[0x2F]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    getCPURAM()[0x80] = 0x3;

    doInstruction(Instruction::DEC, Instruction::ZeroPage, 0x80);

    EXPECT_EQ(0x2, getCPURAM()[0x80]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    getCPURAM()[0x2FF] = 0xFF;

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x0F);
    doInstruction(Instruction::DEC, Instruction::AbsoluteX, 0x2F0);

    EXPECT_EQ(0xFE, getCPURAM()[0x2FF]);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);
}

TEST_F(CPUTest, testTAX)
{
    doInstruction(Instruction::LDX, Instruction::Immediate, 0x1);

    EXPECT_EQ(0x1, getCPUState().x);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::TAX, Instruction::Implied, 0x0);

    EXPECT_EQ(0x0, getCPUState().x);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0xFF);
    doInstruction(Instruction::TAX, Instruction::Implied, 0x0);

    EXPECT_EQ(0xFF, getCPUState().x);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);
}

TEST_F(CPUTest, testCMP)
{
    doInstruction(Instruction::LDA, Instruction::Immediate, 0x5);
    doInstruction(Instruction::CMP, Instruction::Immediate, 0x5);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x5);
    doInstruction(Instruction::CMP, Instruction::Immediate, 0x3);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x3);
    doInstruction(Instruction::CMP, Instruction::Immediate, 0x5);
    EXPECT_FALSE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);
}

TEST_F(CPUTest, testCPX)
{
    doInstruction(Instruction::LDX, Instruction::Immediate, 0x5);
    doInstruction(Instruction::CPX, Instruction::Immediate, 0x5);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x5);
    doInstruction(Instruction::CPX, Instruction::Immediate, 0x3);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LDX, Instruction::Immediate, 0x3);
    doInstruction(Instruction::CPX, Instruction::Immediate, 0x5);
    EXPECT_FALSE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);
}

TEST_F(CPUTest, testCPY)
{
    doInstruction(Instruction::LDY, Instruction::Immediate, 0x5);
    doInstruction(Instruction::CPY, Instruction::Immediate, 0x5);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    getCPURAM()[0x2FF] = 0xFF;

    doInstruction(Instruction::LDY, Instruction::Immediate, 0xFE);
    doInstruction(Instruction::CPY, Instruction::Absolute, 0x2FF);
    EXPECT_FALSE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().negative);

    getCPURAM()[0x2F] = 0xFE;

    doInstruction(Instruction::LDY, Instruction::Immediate, 0xFF);
    doInstruction(Instruction::CPY, Instruction::ZeroPage, 0x2F);
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
}

TEST_F(CPUTest, testAbsoluteY)
{
    getCPURAM()[0x02FF] = 0x5;

    doInstruction(Instruction::LDY, Instruction::Immediate, 0xF);
    doInstruction(Instruction::LDX, Instruction::AbsoluteY, 0x02F0);

    EXPECT_EQ(0x5, getCPUState().x);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x6);
    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::SBC, Instruction::AbsoluteY, 0x02F0);

    EXPECT_EQ(0x1, getCPUState().a);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_TRUE(getCPUState().carry);

    doInstruction(Instruction::STA, Instruction::AbsoluteY, 0x02F0);
    EXPECT_EQ(0x1, getCPURAM()[0x02FF]);
}

TEST_F(CPUTest, testSTYZeroPageX)
{
    doInstruction(Instruction::LDY, Instruction::Immediate, 0x5);
    doInstruction(Instruction::LDX, Instruction::Immediate, 0xF);
    doInstruction(Instruction::STY, Instruction::ZeroPageX, 0xF0);

    EXPECT_EQ(0x5, getCPURAM()[0xFF]);
}

TEST_F(CPUTest, testStackProcessorStatus)
{
    doInstruction(Instruction::SEC, Instruction::Implied, 0);
    doInstruction(Instruction::SEI, Instruction::Implied, 0);
    getCPUState().decimalMode = true;
    getCPUState().negative = true;

    EXPECT_TRUE(getCPUState().carry);
    EXPECT_TRUE(getCPUState().interruptDisabled);
    EXPECT_TRUE(getCPUState().decimalMode);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_FALSE(getCPUState().breakCommand);

    doInstruction(Instruction::PHP, Instruction::Implied, 0);

    // PHP shouldn't change the status
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_TRUE(getCPUState().interruptDisabled);
    EXPECT_TRUE(getCPUState().decimalMode);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_FALSE(getCPUState().breakCommand);

    // Change status so PLP can be noticed
    doInstruction(Instruction::CLC, Instruction::Implied, 0);
    doInstruction(Instruction::CLD, Instruction::Implied, 0);
    getCPUState().interruptDisabled = false;
    getCPUState().negative = false;
    getCPUState().zero = true;
    getCPUState().overflow = true;
    getCPUState().breakCommand = true;

    EXPECT_FALSE(getCPUState().carry);
    EXPECT_FALSE(getCPUState().interruptDisabled);
    EXPECT_FALSE(getCPUState().decimalMode);
    EXPECT_FALSE(getCPUState().negative);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().overflow);
    EXPECT_TRUE(getCPUState().breakCommand);

    doInstruction(Instruction::PLP, Instruction::Implied, 0);

    // Status should be same as before PHP
    EXPECT_TRUE(getCPUState().carry);
    EXPECT_TRUE(getCPUState().interruptDisabled);
    EXPECT_TRUE(getCPUState().decimalMode);
    EXPECT_TRUE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().overflow);
    EXPECT_FALSE(getCPUState().breakCommand);
}

TEST_F(CPUTest, testBIT)
{
    getCPURAM()[0xFF] = 0x5;

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x4);

    // Check that state changes
    getCPUState().zero = true;
    getCPUState().negative = true;
    getCPUState().overflow = true;

    doInstruction(Instruction::BIT, Instruction::ZeroPage, 0xFF);

    EXPECT_FALSE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().overflow);

    getCPURAM()[0x02FF] = 0x80;

    doInstruction(Instruction::BIT, Instruction::Absolute, 0x2FF);

    EXPECT_TRUE(getCPUState().negative);
    EXPECT_TRUE(getCPUState().zero);
    EXPECT_FALSE(getCPUState().overflow);

    getCPURAM()[0x02FF] = 0xc0;

    doInstruction(Instruction::LDA, Instruction::Immediate, 0x80);
    doInstruction(Instruction::BIT, Instruction::Absolute, 0x2FF);

    EXPECT_TRUE(getCPUState().negative);
    EXPECT_FALSE(getCPUState().zero);
    EXPECT_TRUE(getCPUState().overflow);
}

TEST_F(CPUTest, testROMAddressTranslation16KB)
{
    setCPUPrgSize(0x4000);

    EXPECT_EQ(0x0,
              CPUAddressFromROM(0xC000));

    EXPECT_EQ(0x1,
              CPUAddressFromROM(0xC001));

    EXPECT_EQ(0x3FFF,
              CPUAddressFromROM(0xFFFF));
}

TEST_F(CPUTest, testROMAddressTranslation32KB)
{
    setCPUPrgSize(0x8000);

    EXPECT_EQ(0x4000,
              CPUAddressFromROM(0xC000));

    EXPECT_EQ(0x0,
              CPUAddressFromROM(0x8000));

    EXPECT_EQ(0x1,
              CPUAddressFromROM(0x8001));

    EXPECT_EQ(0x3000,
              CPUAddressFromROM(0xB000));

    EXPECT_EQ(0x7FFF,
              CPUAddressFromROM(0xFFFF));
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
