#pragma once

#include "windowBase.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

class WindowMock : public WindowBase {
public:
    MOCK_METHOD3(draw, bool(const std::array<uint8_t, 256> & oamData,
                            const std::array<uint8_t, 16383> & vram,
                            const PPU::ControlStatus & attributes));

    MOCK_METHOD2(initTextures, bool(const uint8_t * chrStart,
                                    size_t chrSize));

};
