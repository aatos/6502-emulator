#include "ines.h"

#include <string>
#include <fstream>
#include <gtest/gtest.h>

class INesTest : public ::testing::Test {
protected:
    INesTest() {
    }

    void getFileWithHeader(const std::string & header, std::ifstream & ifs) {
        std::ofstream out_stream("/tmp/test.nes");
        // FIXME: wow, something happened "ASSERT_TRUE(out_stream);" stopped
        // compiling out of the blue
        if (!out_stream)
            FAIL();

        out_stream << header;
        out_stream.close();

        ifs.open("/tmp/test.nes");
        if (!ifs)
            FAIL();
    }

    virtual ~INesTest() {
    }
};

TEST_F(INesTest, parseHeaderTestSuccessful)
{
    std::string header = {'N', 'E', 'S', '\x1A',
                          '\2', '\3', '\0', '\0',
                          '\0', '\0', '\0', '\0',
                          '\0', '\0', '\0', '\0'
    };

    std::ifstream stream;
    getFileWithHeader(header, stream);

    INesHeader ines;
    ASSERT_EQ(0, ines.parseHeader(stream));

    EXPECT_EQ(2, ines.prg_size_);
    EXPECT_EQ(3, ines.chr_size_);
    EXPECT_FALSE(ines.vertical_mirroring_);

    stream.close();
}

TEST_F(INesTest, parseHeaderBadMagic)
{
    std::string header = {'N', 'E', 'S', '\x1B',
                          '\2', '\3', '\0', '\0',
                          '\0', '\0', '\0', '\0',
                          '\0', '\0', '\0', '\0'
    };

    std::ifstream stream;
    getFileWithHeader(header, stream);

    INesHeader ines;
    ASSERT_EQ(-1, ines.parseHeader(stream));

    stream.close();
}

TEST_F(INesTest, parseHeaderGetVerticalMirroring)
{
    std::string header = {'N', 'E', 'S', '\x1A',
                          '\1', '\2', '\1', '\0',
                          '\0', '\0', '\0', '\0',
                          '\0', '\0', '\0', '\0'
    };

    std::ifstream stream;
    getFileWithHeader(header, stream);

    INesHeader ines;
    ASSERT_EQ(0, ines.parseHeader(stream));

    EXPECT_EQ(1, ines.prg_size_);
    EXPECT_EQ(2, ines.chr_size_);
    EXPECT_TRUE(ines.vertical_mirroring_);

    stream.close();
}

TEST_F(INesTest, parseHeaderTooShort)
{
    std::string header = {'N', 'E', 'S', '\x1A',
                          '\1', '\2', '\1', '\0'
    };

    std::ifstream stream;
    getFileWithHeader(header, stream);

    INesHeader ines;
    ASSERT_EQ(-1, ines.parseHeader(stream));

    stream.close();
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
