CXX := clang++
CXXFLAGS := -std=c++14
CXXFLAGS += -Wall -Wextra -pedantic
CXXFLAGS += -Wformat=2 -Wswitch-default -Wcast-align -Wpointer-arith -Winline
CXXFLAGS += -Wshadow -Wwrite-strings -Wconversion -Wstrict-aliasing=2
CXXFLAGS += -Wundef -Wcast-qual -Wunused-result
CXXFLAGS += -g

ifeq ($(CXX), clang++)
    # Clang specific
    CXXFLAGS += -Weverything -Wno-padded -Wno-c++98-compat -Wno-c++98-compat-pedantic
    # Disable these for now
    CXXFLAGS += -Wno-exit-time-destructors -Wno-switch-enum -Wno-covered-switch-default
    CXXFLAGS += -Wno-disabled-macro-expansion

    ifdef USE_SAN
        # FIXME: gdb doesn't seem to get all symbols with this
        CXXFLAGS += -fsanitize=address,undefined
        LDFLAGS += -fsanitize=address,undefined

        # Address sanitizer requires clang as linker
        LD := clang
    endif
endif

ifdef DEBUG_INSTR
	CXXFLAGS += -DDEBUG_INSTR
endif

OBJS := src/main.o src/ines.o
OBJS += src/instruction.o
OBJS += src/cpu.o
OBJS += src/ppu.o
OBJS += src/windowSFML.o
OBJS += src/timer.o
OBJS += src/controllers.o
OBJS += src/apu.o

INC := -Isrc/

LDLIBS := -lsfml-graphics -lsfml-window -lsfml-system -lrt

TEST_CXXFLAGS := -Wno-global-constructors -Wno-exit-time-destructors -Wno-weak-vtables

TEST_INC := -Itest/

TEST_BINS := test/ines_test test/cpu_test
TEST_BINS += test/ppu_test

TEST_LDLIBS := -lgtest -lgmock -lpthread # FIXME: Why do you need pthread

all: emu

emu: $(OBJS)
	$(CXX) $(LDFLAGS) $^ -o $@ $(LDLIBS)

test/cpu_test: test/cpu_test.cc src/cpu.o src/instruction.o src/ppu.o src/controllers.o src/apu.o

.PHONY: test
test: $(TEST_BINS)
	for test in $^; \
		do $$test || exit 1; \
	done
	cppcheck --enable=all src
	make clean
	scan-build --use-c++=clang++ -enable-checker alpha,core,security,unix --status-bugs make -j4

%.o: %.cc %.hh
	$(CXX) $(INC) $(CXXFLAGS) $(LDFLAGS) $< -c -o $@

test/%_test: test/%_test.cc src/%.o
	$(CXX) $(INC) $(TEST_INC) $(CXXFLAGS) $(TEST_CXXFLAGS) $(LDFLAGS) $^ -o $@ $(LDLIBS) $(TEST_LDLIBS)

.PHONY: clean
clean:
	rm -f src/*.o TAGS emu $(TEST_BINS)

.PHONY: tags
tags:
	rm -f TAGS
	find . -type f -iname "*.cc" -o -iname "*.hh" -o -iname "*.[ch]" | xargs etags -a
