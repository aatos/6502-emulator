import os

sources = [ "main", "ines", "instruction", "cpu", "ppu", "windowSFML",
            "controllers", "apu" ]

# Setup default environment used by both the emulator and the tests
def setup_default_env():
    def set_scan_build_vars(env):
        for c in ["CXX", "CC"]:
            if os.getenv(c):
                env[c] = os.getenv(c)
        env["ENV"].update(v for v in os.environ.items() if v[0].startswith("CCC_"))

    if "gcc" in ARGUMENTS:
        cc = "gcc"
        cxx = "g++"
    else:
        cc = "clang"
        cxx = "clang++"

    default_env = DefaultEnvironment(CXX=cxx,
                                     CC=cc,
                                     CXXFLAGS="-std=c++14 -Wall -Wextra -pedantic -Wformat=2 "
                                     "-Wswitch-default -Wcast-align -Wpointer-arith "
                                     "-Winline -Wshadow -Wwrite-strings -Wconversion "
                                     "-Wstrict-aliasing=2 -Wundef -Wcast-qual -Wunused-result ",
                                     CPPPATH=os.path.join("#", "src"))

    # Scan-build sets some environment variables, which are by default ignored
    # by Scons, so set them up as described in http://stackoverflow.com/a/9305378.
    set_scan_build_vars(default_env)

    if default_env["CXX"].startswith("clang"):
        default_env.Append(CXXFLAGS="-Weverything -Wno-padded -Wno-c++98-compat "
                           "-Wno-c++98-compat-pedantic ")
        default_env.Append(CXXFLAGS="-Wno-disabled-macro-expansion -Wno-covered-switch-default "
                           "-Wno-exit-time-destructors -Wno-packed ")
    elif default_env["CXX"].startswith("g++"):
        default_env.Append(CXXFLAGS="-Wshift-negative-value -Wshift-overflow -Wnull-dereference "
                           "-Wduplicated-cond ")
        default_env.Append(CXXFLAGS="-Wno-switch-default ")
        # GCC's -Wconversion is not very useful: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=40752
        default_env.Append(CXXFLAGS="-Wno-conversion ")
    disabled_warnings = "-Wno-switch-enum -Wno-padded "
    default_env.Append(CXXFLAGS=disabled_warnings)

    if "use_san" in ARGUMENTS:
        if not default_env["CXX"].startswith("clang"):
            print("Cannot use sanitizers without clang, exiting..")
            Exit(1)

        # FIXME: gdb doesn't seem to get all symbols with this
        sanitizers = "-fsanitize=address,undefined "
        default_env.Append(CXXFLAGS=sanitizers, LINKFLAGS=sanitizers)
        # FIXME: add clang as the linker, does not work: 'sh: o: command not found'
        #default_env.Append(LINK="clang")
        ARGUMENTS["skip_valgrind"] = '1'

    if "release" in ARGUMENTS:
        default_env.Append(CXXFLAGS="-O2 ")
    else:
        default_env.Append(CXXFLAGS="-g ")

    if "print_inst" in ARGUMENTS:
        default_env.Append(CXXFLAGS="-DDEBUG_INSTR ")

    SetOption("num_jobs", 4)

    return default_env

default_env = setup_default_env()

Help("""
=========== 6502-emulator ===========
'scons [emu]' to build the emulator.
'scons test' to build the unit tests and run them, cppcheck and scan-build.

Add 'release=1' for optimization and not including the debug symbols.
Add 'use_san=1' to compile with clang sanitizers, disables valgrind with tests.
Add 'print_inst=1' to print all executed instructions.
Add 'skip_scanbuild=1' to skip scan-build when testing.
Add 'skip_valgrind=1' to don't use valgrind when testing.
Add 'gcc=1' to compile with gcc instead of clang.
""")

sfml_libs = ["sfml-graphics", "sfml-window", "sfml-system"]
required_libs = sfml_libs
if default_env['PLATFORM'] != 'darwin':
    required_libs.append('rt')
emu_env = default_env.Clone(LIBS=required_libs)

emu = emu_env.Program("emu", [ os.path.join("src", f + ".cc") for f in sources ])
Default(emu)

if not emu_env.GetOption('clean') and not emu_env.GetOption('help'):
    if "emu" in map(str, BUILD_TARGETS):
        conf = Configure(emu_env)
        for lib in sfml_libs:
            if not conf.CheckLib(lib):
                print("SFML library not found, exiting..")
                Exit(1)
        emu_env = conf.Finish()

SConscript([os.path.join("test", "SConscript")])
