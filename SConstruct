import os

sources = [ "main", "ines", "instruction", "cpu", "ppu", "windowSFML", "timer",
            "controllers", "apu" ]

# Setup default environment used by both the emulator and the tests
def setup_default_env():
    default_env = DefaultEnvironment(CXX="clang++",
                                     CXXFLAGS="-std=c++14 -Wall -Wextra -pedantic -Wformat=2 "
                                     "-Wswitch-default -Wcast-align -Wpointer-arith "
                                     "-Winline -Wshadow -Wwrite-strings -Wconversion "
                                     "-Wstrict-aliasing=2 -Wundef -Wcast-qual -Wunused-result ",
                                     CPPPATH=os.path.join("#", "src"))

    if default_env["CXX"].startswith("clang"):
        default_env.Append(CXXFLAGS="-Weverything -Wno-padded -Wno-c++98-compat "
                           "-Wno-c++98-compat-pedantic ")

    disabled_warnings = "-Wno-exit-time-destructors -Wno-switch-enum " \
                        "-Wno-covered-switch-default -Wno-disabled-macro-expansion "
    default_env.Append(CXXFLAGS=disabled_warnings)

    if "use_san" in ARGUMENTS:
        # FIXME: gdb doesn't seem to get all symbols with this
        default_env.Append(CXXFLAGS="-fsanitize=address,undefined",
                           LINKFLAGS="-fsanitize=address,undefined")
        # FIXME: add clang as the linker, does not work: 'sh: o: command not found'
        #default_env.Append(LINK="clang")

    if not "release" in ARGUMENTS:
        default_env.Append(CXXFLAGS="-g ")

    if "print_inst" in ARGUMENTS:
        default_env.Append(CXXFLAGS="-DDEBUG_INSTR ")

    SetOption("num_jobs", 4)

    return default_env

default_env = setup_default_env()

Help("""
=========== 6502-emulator ===========
'scons [emu]' to build the emulator.
'scons test' to build and run the tests.

Add 'release=1' for not including the debug symbols.
Add 'use_san=1' to compile with clang sanitizers.
Add 'print_inst=1' to print all executed instructions.
""")

required_sfml_libs = ["sfml-graphics", "sfml-window", "sfml-system"]
emu_env = default_env.Clone(LIBS=(["rt"] + required_sfml_libs))

emu = emu_env.Program("emu", [ os.path.join("src", f + ".cc") for f in sources ])
Default(emu)

if not emu_env.GetOption('clean'):
    if "emu" in map(str, BUILD_TARGETS):
        conf = Configure(emu_env)
        for lib in required_sfml_libs:
            if not conf.CheckLib(lib):
                print("SFML library not found, exiting..")
                Exit(1)
        emu_env = conf.Finish()

SConscript([os.path.join("test", "SConscript")])
